USE [master]
GO
/****** Object:  Database [DORA_DB]    Script Date: 19/06/16 02:23:37 ******/
CREATE DATABASE [DORA_DB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DORA_DB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\DORA_DB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DORA_DB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\DORA_DB_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DORA_DB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DORA_DB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DORA_DB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DORA_DB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DORA_DB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DORA_DB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DORA_DB] SET ARITHABORT OFF 
GO
ALTER DATABASE [DORA_DB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DORA_DB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DORA_DB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DORA_DB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DORA_DB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DORA_DB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DORA_DB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DORA_DB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DORA_DB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DORA_DB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DORA_DB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DORA_DB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DORA_DB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DORA_DB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DORA_DB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DORA_DB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DORA_DB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DORA_DB] SET RECOVERY FULL 
GO
ALTER DATABASE [DORA_DB] SET  MULTI_USER 
GO
ALTER DATABASE [DORA_DB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DORA_DB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DORA_DB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DORA_DB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DORA_DB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DORA_DB]
GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Function [dbo].[Split]    Script Date: 04/05/16 17:25:47 ******/

CREATE FUNCTION [dbo].[Split] (
	@InputString nvarchar(MAX),
	@Delimiter nvarchar(50)
)
RETURNS @Items TABLE (
	Nr INT IDENTITY(1,1),
	Item nvarchar(MAX)
)
AS
BEGIN
	IF @Delimiter = ' '
	BEGIN
		SET @Delimiter = ','
		SET @InputString = REPLACE(@InputString, ' ', @Delimiter)
	END

	IF (@Delimiter IS NULL OR @Delimiter = '')
		SET @Delimiter = ','

	DECLARE @Item nvarchar(MAX)
	DECLARE @ItemList nvarchar(MAX)
	DECLARE @DelimIndex INT

	SET @ItemList = @InputString
	SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
	WHILE (@DelimIndex != 0)
	BEGIN
		SET @Item = SUBSTRING(@ItemList, 0, @DelimIndex)
		INSERT INTO @Items VALUES (@Item)

		SET @ItemList = SUBSTRING(@ItemList, @DelimIndex+1, LEN(@ItemList)-@DelimIndex)
		SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
	END

	IF @Item IS NOT NULL
	BEGIN
		SET @Item = @ItemList
		INSERT INTO @Items VALUES (@Item)
	END
	ELSE
		INSERT INTO @Items VALUES (@InputString)

	RETURN
END


GO
/****** Object:  Table [dbo].[Foto]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Foto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[latitude] [float] NOT NULL,
	[longitude] [float] NOT NULL,
	[foto] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_Foto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ponto]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ponto](
	[latitude] [float] NOT NULL,
	[longitude] [float] NOT NULL,
 CONSTRAINT [PK_Ponto] PRIMARY KEY CLUSTERED 
(
	[latitude] ASC,
	[longitude] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ponto_Sessao]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ponto_Sessao](
	[ordem] [int] NULL,
	[Pontolatitude] [float] NOT NULL,
	[Pontolongitude] [float] NOT NULL,
	[Sessaocodigo] [nvarchar](11) NOT NULL
 CONSTRAINT [PK_Ponto_Sessao] PRIMARY KEY CLUSTERED 
(
	[Pontolatitude] ASC,
	[Pontolongitude] ASC,
	[Sessaocodigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sessao]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sessao](
	[codigo] [nvarchar](11) NOT NULL,
	[pontuacao] [int] NOT NULL,
	[data] [date] NULL,
	[registo_de_voz] [varbinary](max) NULL,
	[relatorio] [nvarchar](max) NOT NULL,
	[Utilizadoremail] [nvarchar](50) NOT NULL,
	[distancia] [float] NOT NULL,
 CONSTRAINT [PK_Sessao] PRIMARY KEY CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Utilizador]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Utilizador](
	[email] [nvarchar](50) NOT NULL,
	[telefone] [nvarchar](11) NULL,
	[facebook] [nvarchar](100) NULL,
	[password] [nvarchar](32) NOT NULL,
	[data_nascimento] [date] NOT NULL,
	[nome] [nvarchar](100) NOT NULL,
	[rua] [nvarchar](100) NOT NULL,
	[localidade] [nvarchar](100) NOT NULL,
	[latitude] [float] NOT NULL,
	[longitude] [float] NOT NULL,
	[ranking] [nvarchar](17) NOT NULL,
	[imagem_de_perfil] [varbinary](max) NULL,
	[pontuacao_geral] [int] NOT NULL,
	[logged_in] [bit] NOT NULL,
 CONSTRAINT [PK_Utilizador] PRIMARY KEY CLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Utilizador_Ponto]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilizador_Ponto](
	[Utilizadoremail] [nvarchar](50) NOT NULL,
	[Pontolatitude] [float] NOT NULL,
	[Pontolongitude] [float] NOT NULL,
	[nome_do_local] [nvarchar](100) NULL,
	[descricao] [nvarchar](max) NULL,
	[primeiro] [bit] NOT NULL,
 CONSTRAINT [PK_Utilizador_Ponto] PRIMARY KEY CLUSTERED 
(
	[Utilizadoremail] ASC,
	[Pontolatitude] ASC,
	[Pontolongitude] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  View [dbo].[melhor_explorador]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[melhor_explorador]    Script Date: 04/05/16 17:25:47 ******/

CREATE VIEW [dbo].[melhor_explorador]
AS
	SELECT Utilizadoremail FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY COUNT(Utilizadoremail)) Posicao, Utilizadoremail
			FROM Utilizador_Ponto
			GROUP BY Utilizadoremail
  ) Aux WHERE Aux.Posicao = 1


GO
ALTER TABLE [dbo].[Utilizador] ADD  CONSTRAINT [DF__Utilizado__pontu__1273C1CD]  DEFAULT ((0)) FOR [pontuacao_geral]
GO
ALTER TABLE [dbo].[Utilizador] ADD  CONSTRAINT [DF_Utilizador_logged_in]  DEFAULT ((0)) FOR [logged_in]
GO
ALTER TABLE [dbo].[Foto]  WITH CHECK ADD  CONSTRAINT [FK_Foto_Utilizador_Ponto] FOREIGN KEY([email], [latitude], [longitude])
REFERENCES [dbo].[Utilizador_Ponto] ([Utilizadoremail], [Pontolatitude], [Pontolongitude])
GO
ALTER TABLE [dbo].[Foto] CHECK CONSTRAINT [FK_Foto_Utilizador_Ponto]
GO
ALTER TABLE [dbo].[Ponto_Sessao]  WITH CHECK ADD  CONSTRAINT [FK_Ponto_Sessao_Ponto] FOREIGN KEY([Pontolatitude], [Pontolongitude])
REFERENCES [dbo].[Ponto] ([latitude], [longitude])
GO
ALTER TABLE [dbo].[Ponto_Sessao] CHECK CONSTRAINT [FK_Ponto_Sessao_Ponto]
GO
ALTER TABLE [dbo].[Ponto_Sessao]  WITH CHECK ADD  CONSTRAINT [FK_Ponto_Sessao_Sessao] FOREIGN KEY([Sessaocodigo])
REFERENCES [dbo].[Sessao] ([codigo])
GO
ALTER TABLE [dbo].[Ponto_Sessao] CHECK CONSTRAINT [FK_Ponto_Sessao_Sessao]
GO
ALTER TABLE [dbo].[Sessao]  WITH CHECK ADD  CONSTRAINT [FK_Sessao_Utilizador] FOREIGN KEY([Utilizadoremail])
REFERENCES [dbo].[Utilizador] ([email])
GO
ALTER TABLE [dbo].[Sessao] CHECK CONSTRAINT [FK_Sessao_Utilizador]
GO
ALTER TABLE [dbo].[Utilizador_Ponto]  WITH CHECK ADD  CONSTRAINT [FK_Utilizador_Ponto_Ponto] FOREIGN KEY([Pontolatitude], [Pontolongitude])
REFERENCES [dbo].[Ponto] ([latitude], [longitude])
GO
ALTER TABLE [dbo].[Utilizador_Ponto] CHECK CONSTRAINT [FK_Utilizador_Ponto_Ponto]
GO
ALTER TABLE [dbo].[Utilizador_Ponto]  WITH CHECK ADD  CONSTRAINT [FK_Utilizador_Ponto_Utilizador] FOREIGN KEY([Utilizadoremail])
REFERENCES [dbo].[Utilizador] ([email])
GO
ALTER TABLE [dbo].[Utilizador_Ponto] CHECK CONSTRAINT [FK_Utilizador_Ponto_Utilizador]
GO
/****** Object:  StoredProcedure [dbo].[adicionar_pontos]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[adicionar_pontos]    Script Date: 04/05/16 17:25:47 ******/

CREATE PROCEDURE [dbo].[adicionar_pontos]
	@user nvarchar(31),
	@sessao nvarchar(11),
	@latitudes nvarchar(MAX),
	@longitudes nvarchar(MAX),
	@nomes nvarchar(MAX),
	@descricoes nvarchar(MAX)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			-- adicionar pontos
			DECLARE @tabela_pontos TABLE(
				Nr INT,
				Latitude FLOAT NULL,
				Longitude FLOAT NULL,
				primeiro BIT DEFAULT 0
			)

			INSERT INTO @tabela_pontos
				(Nr,Latitude,Longitude)
				SELECT A.Nr, CONVERT( FLOAT,A.Item ) Latitude, CONVERT( FLOAT,B.Item ) Longitude
					FROM SPLIT( @latitudes,',' ) A INNER JOIN SPLIT( @longitudes,',' ) B
						ON A.Nr = B.Nr

			INSERT INTO Ponto
				SELECT Latitude, Longitude
					FROM @tabela_pontos

			-- ligar pontos e sessao
			INSERT INTO Ponto_Sessao
				(Pontolatitude, Pontolongitude, Sessaocodigo)
				SELECT Latitude, Longitude, @sessao
					FROM @tabela_pontos

			-- ligar pontos e user
			DECLARE @latitude FLOAT
			DECLARE @longitude FLOAT
			DECLARE pontos_cursor CURSOR FOR
				SELECT Latitude,Longitude
					FROM @tabela_pontos

			OPEN pontos_cursor
			FETCH NEXT FROM pontos_cursor
				INTO @latitude,@longitude

			WHILE @@FETCH_STATUS = 0
			BEGIN
				IF NOT EXISTS
					(SELECT 1
						FROM Utilizador_Ponto
						WHERE Utilizadoremail = @user
							AND Pontolatitude = @latitude
							AND Pontolongitude = @longitude)
					UPDATE @tabela_pontos
						SET primeiro = 1
						WHERE Latitude = @latitude
							AND Longitude = @longitude

				FETCH NEXT FROM pontos_cursor
					INTO @latitude,@longitude
			END
			CLOSE vendor_cursor;
			DEALLOCATE vendor_cursor;

			INSERT INTO Utilizador_Ponto
				SELECT @user, Latitude, Longitude, N.Item, D.Item, T.primeiro
					FROM @tabela_pontos	T
						INNER JOIN SPLIT( @nomes,',' ) N
							ON N.Nr = T.Nr
						INNER JOIN SPLIT( @descricoes,',' ) D
							ON D.Nr = T.Nr
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK
	END CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[adicionar_sessao]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[adicionar_sessao]    Script Date: 04/05/16 17:25:47 ******/

CREATE PROCEDURE [dbo].[adicionar_sessao]
	@codigo nvarchar(11),
	@pontuacao INT, -- preferivel calcular pontuacao no C# (mais pratico para trabalhar com listas)
	@data date,
	@voz varbinary(MAX),
	@relatorio nvarchar(MAX),
	@user nvarchar(31),
	@latitudes nvarchar(MAX),
	@longitudes nvarchar(MAX),
	@nomes nvarchar(MAX),
	@descricoes nvarchar(MAX),
	@fotos nvarchar(MAX),
	@distance float
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			-- adicionar pontos
			EXEC adicionar_pontos
				@user = @user,
				@sessao = @codigo,
				@latitudes = @latitudes,
				@longitudes =@longitudes,
				@nomes = @nomes,
				@descricoes = @descricoes

			-- adicionar sessao

			INSERT INTO Sessao
				VALUES
				( @codigo,@pontuacao,@data,@voz,@relatorio,@user,@distance )

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK
	END CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[fotos_local]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[fotos_local]    Script Date: 04/05/16 16:24:56 ******/

CREATE PROCEDURE [dbo].[fotos_local]
	@latitude float,
	@longitude float
AS
BEGIN
	SELECT foto Foto
		FROM Foto
		WHERE latitude = @latitude
			AND longitude = @longitude
END


GO
/****** Object:  StoredProcedure [dbo].[local_mais_visitado]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[local_mais_visitado]    Script Date: 04/05/16 16:24:56 ******/

CREATE PROCEDURE [dbo].[local_mais_visitado]
AS
BEGIN
	SELECT Latitude, Longitude, Visitas
		FROM (
			SELECT 	ROW_NUMBER() OVER (ORDER BY COUNT(Utilizadoremail) DESC ) Posicao,
			Pontolatitude Latitude, Pontolongitude Longitude, COUNT(*) Visitas
				FROM Utilizador_Ponto
				GROUP BY Pontolatitude, Pontolongitude
		) Aux WHERE Aux.Posicao = 1
END


GO
/****** Object:  StoredProcedure [dbo].[quadro_pontuacoes]    Script Date: 19/06/16 02:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[quadro_pontuacoes]    Script Date: 04/05/16 15:44:48 ******/

CREATE PROCEDURE [dbo].[quadro_pontuacoes]
AS
BEGIN
	SELECT RANK() OVER (ORDER BY pontuacao_geral DESC) Posicao,
		email Email, pontuacao_geral Pontos, ranking Rank
		FROM Utilizador
		ORDER BY Posicao
END

/****** Object:  Trigger [dbo].[Atualiza_Pontuacao]    Script Date: 19/06/16 02:23:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[Atualiza_Pontuacao]
	ON [dbo].[Sessao]
	AFTER INSERT, UPDATE, DELETE
	AS
		UPDATE Utilizador
		SET pontuacao_geral = (SELECT COALESCE(SUM(pontuacao),0) FROM Sessao AS S WHERE email=S.Utilizadoremail)


GO
		
