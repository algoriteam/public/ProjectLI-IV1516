﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Business;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace Data.DAO
{
    public class UserDAO : IDictionary<string, User>
    {
        public User this[string key]
        {
            get
            {
                try
                {
                    if (key == null)
                        throw new ArgumentNullException("Key is null");
                    else
                    {
                        Utilizador aux = new Utilizador();

                        using (DORA_DBDataContext db = new DORA_DBDataContext())
                        {
                            var users = from user in db.Utilizadors where user.email.Equals(key) select user;

                            if (!users.Any())
                                throw new KeyNotFoundException("Key not found");
                            else
                                aux = users.First();
                        }
                        return new User(aux.nome, aux.email, aux.facebook, aux.telefone, aux.password, aux.data_nascimento,
                                Ranking_Solver.FromDescription(aux.ranking),
                                (aux.imagem_de_perfil != null ? aux.imagem_de_perfil.ToArray() : null), 
                                aux.pontuacao_geral, aux.rua, aux.localidade,
                                aux.logged_in, aux.latitude, aux.longitude);
                    }
                }
                catch(System.Data.SqlClient.SqlException e)
                {
                    return null;
                }
            }

            set
            {
                if (key == null)
                    throw new ArgumentNullException("Key is null");
                else if (this.IsReadOnly == true)
                    throw new NotSupportedException("The Table is read-only");
                else
                {
                    using (DORA_DBDataContext db = new DORA_DBDataContext())
                    {
                        var users = from user in db.Utilizadors where user.email.Equals(key) select user;

                        if (!users.Any())
                            throw new KeyNotFoundException("Key not found");
                        else
                        {
                            foreach (Utilizador aux in users)
                            {
                                aux.nome = value.Name;
                                aux.email = value.Email;
                                aux.facebook = value.Facebook;
                                aux.telefone = value.Phone;
                                aux.password = value.Password;
                                aux.data_nascimento = value.Birthday;
                                aux.imagem_de_perfil = ImageSourceToBytes(new JpegBitmapEncoder(), value.Image);
                                aux.ranking = Ranking_Solver.FromEnum(value.Ranking);
                                aux.pontuacao_geral = value.Experience;
                                aux.rua = value.Street;
                                aux.localidade = value.Locality;
                                aux.logged_in = value.Logged_in;
                                aux.latitude = value.Latitude;
                                aux.longitude = value.Longitude;
                            }
                            db.SubmitChanges();
                        }
                    }
                }
            }
        }

        public byte[] ImageSourceToBytes(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }
            return bytes;
        }

        public int Count
        {
            get
            {
                int aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Utilizadors.Count();
                }
                return aux;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                bool aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Utilizadors.IsReadOnly;
                }
                return aux;
            }
        }

        public ICollection<string> Keys
        {
            get
            {
                List<string> emails = new List<string>();
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from user in db.Utilizadors select user.email;
                    foreach (string email in users)
                        emails.Add(email);
                }
                return emails;
            }
        }

        public ICollection<User> Values
        {
            get
            {
                List<User> users = new List<User>();
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var user = from aux in db.Utilizadors select aux;
                    foreach (Utilizador aux in user)
                        users.Add(new User(aux.nome, aux.email, aux.facebook, aux.telefone, aux.password, aux.data_nascimento, 
                                   Ranking_Solver.FromDescription(aux.ranking),
                                   (aux.imagem_de_perfil != null ? aux.imagem_de_perfil.ToArray() : null), 
                                   aux.pontuacao_geral, aux.rua, aux.localidade, 
                                   aux.logged_in, aux.latitude, aux.longitude));
                }
                return users;
            }
        }

        public void Add(KeyValuePair<string, User> item)
        {
            this.Add(item.Key, item.Value);
        }

        public void Add(string key, User value)
        {
            if (key == null)
                throw new ArgumentNullException("Key is null");
            else if (this.IsReadOnly == true)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from user in db.Utilizadors where user.email.Equals(key) select user;

                    if (users.Any())
                        throw new ArgumentException("An Element with the same Key already exists");
                    else
                    {
                        Utilizador aux = new Utilizador();
                        aux.nome = value.Name;
                        aux.email = value.Email;
                        aux.facebook = value.Facebook;
                        aux.telefone = value.Phone;
                        aux.password = value.Password;
                        aux.data_nascimento = value.Birthday;
                        aux.imagem_de_perfil = ImageSourceToBytes(new JpegBitmapEncoder(), value.Image);
                        aux.ranking = Ranking_Solver.FromEnum(value.Ranking);
                        aux.pontuacao_geral = value.Experience;
                        aux.rua = value.Street;
                        aux.localidade = value.Locality;
                        aux.logged_in = value.Logged_in;
                        aux.latitude = value.Latitude;
                        aux.longitude = value.Longitude;

                        db.Utilizadors.InsertOnSubmit(aux);
                        db.SubmitChanges();
                    }
                }
            }
        }

        public void Clear()
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                    db.ExecuteCommand("DELETE FROM Utilizador");
            }
        }

        public bool Contains(KeyValuePair<string, User> item)
        {
            bool exists;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from user in db.Utilizadors where user.email.Equals(item.Key) select user;

                if (users.Any())
                {
                    Utilizador aux = users.First();
                    User usr = new User(aux.nome, aux.email, aux.facebook, aux.telefone, aux.password, aux.data_nascimento,
                                Ranking_Solver.FromDescription(aux.ranking),
                                (aux.imagem_de_perfil != null ? aux.imagem_de_perfil.ToArray() : null), 
                                aux.pontuacao_geral, aux.rua, aux.localidade, 
                                aux.logged_in, aux.latitude, aux.longitude);

                    if (usr.Equals(item.Value))
                        exists = true;
                    else
                        exists = false;
                }
                else
                    exists = false;
            }
            return exists;
        }

        public bool ContainsKey(string key)
        {
            bool exists;

            if (key == null)
                throw new ArgumentNullException("Key is null");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from user in db.Utilizadors where user.email.Equals(key) select user;

                    if (users.Any())
                        exists = true;
                    else
                        exists = false;
                }
            }
            return exists;
        }

        public void CopyTo(KeyValuePair<string, User>[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("Array is null");
            else if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException("Array Index is less than 0");
            else if ((array.Length - arrayIndex) < this.Count)
                throw new ArgumentException("The number of elements in the source is greater" +
                    "than the available space from arrayIndex to the end of the destination array");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from user in db.Utilizadors select user;

                    foreach (Utilizador aux in users)
                        array[arrayIndex++] = new KeyValuePair<string, User>(aux.email, new User(aux.nome, aux.email, 
                            aux.facebook, aux.telefone, aux.password, aux.data_nascimento, 
                            Ranking_Solver.FromDescription(aux.ranking),
                            (aux.imagem_de_perfil != null ? aux.imagem_de_perfil.ToArray() : null), 
                            aux.pontuacao_geral, aux.rua, aux.localidade, 
                            aux.logged_in, aux.latitude, aux.longitude));
                }
            }
        }

        public IEnumerator<KeyValuePair<string, User>> GetEnumerator()
        {
            IEnumerator<KeyValuePair<string, User>> res;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                List<KeyValuePair<string, User>> aux = new List<KeyValuePair<string, User>>();
                var users = from user in db.Utilizadors select user;

                foreach (Utilizador user in users)
                    aux.Add(new KeyValuePair<string, User>(user.email, new User(user.nome, user.email, user.facebook,
                             user.telefone, user.password, user.data_nascimento,
                             Ranking_Solver.FromDescription(user.ranking),
                             (user.imagem_de_perfil != null ? user.imagem_de_perfil.ToArray() : null), 
                             user.pontuacao_geral, user.rua, user.localidade, 
                             user.logged_in, user.latitude, user.longitude)));

                res = aux.GetEnumerator();
            }
            return res;
        }

        public bool Remove(KeyValuePair<string, User> item)
        {
            return this.Remove(item.Key);
        }

        public bool Remove(string key)
        {
            bool aux;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from user in db.Utilizadors where user.email.Equals(key) select user;

                if (!users.Any())
                    aux = false;
                else
                {
                    foreach (Utilizador user in users)
                        db.Utilizadors.DeleteOnSubmit(user);
                    db.SubmitChanges();
                    aux = true;
                }
            }

            return aux;
        }

        public bool TryGetValue(string key, out User value)
        {
            if (key == null)
                throw new ArgumentNullException("Key is null");
            else
            {
                bool aux;

                try
                {
                    value = this[key];
                    aux = true;
                }
                catch (KeyNotFoundException e)
                {
                    value = new User();
                    aux = false;
                }
                return aux;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
