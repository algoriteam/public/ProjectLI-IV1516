﻿using Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Data.DAO
{
    public class UserPointDAO : IDictionary<UserCoordinates, UserPoint>
    {
        public UserPoint this[UserCoordinates key]
        {
            get
            {
                if (key == null)
                    throw new ArgumentNullException("Key is null");
                else
                {
                    Utilizador_Ponto aux = new Utilizador_Ponto();

                    using (DORA_DBDataContext db = new DORA_DBDataContext())
                    {
                        var points = from point 
                                    in db.Utilizador_Pontos
                                    where point.Utilizadoremail.Equals(key.Email) &&
                                          point.Pontolatitude == key.Coordinates.Latitude &&
                                          point.Pontolongitude == key.Coordinates.Longitude
                                    select point;

                        if (!points.Any())
                            throw new KeyNotFoundException("Key not found");
                        else
                            aux = points.First();
                    }
                    return new UserPoint(new Point(aux.Pontolatitude, aux.Pontolongitude), aux.Utilizadoremail, 
                        aux.nome_do_local, aux.descricao, null);
                }
            }

            set
            {
                if (key == null)
                    throw new ArgumentNullException("Key is null");
                else if (this.IsReadOnly == true)
                    throw new NotSupportedException("The Table is read-only");
                else
                {
                    using (DORA_DBDataContext db = new DORA_DBDataContext())
                    {
                        var users = from point
                                    in db.Utilizador_Pontos
                                    where point.Utilizadoremail.Equals(key.Email) &&
                                          point.Pontolatitude == key.Coordinates.Latitude &&
                                          point.Pontolongitude == key.Coordinates.Longitude
                                    select point;

                        if (!users.Any())
                            throw new KeyNotFoundException("Key not found");
                        else
                        {
                            foreach (Utilizador_Ponto aux in users)
                            {
                                aux.Utilizadoremail = key.Email;
                                aux.Pontolatitude = key.Coordinates.Latitude;
                                aux.Pontolongitude = key.Coordinates.Longitude;
                                aux.nome_do_local = value.Name;
                                aux.descricao = value.Description;
                                aux.primeiro = value.First;
                            }
                            db.SubmitChanges();
                        }
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                int aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Utilizador_Pontos.Count();
                }
                return aux;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                bool aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Utilizador_Pontos.IsReadOnly;
                }
                return aux;
            }
        }

        public ICollection<UserCoordinates> Keys
        {
            get
            {
                List<UserCoordinates> emails = new List<UserCoordinates>();
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from user in db.Utilizador_Pontos select user;

                    foreach (Utilizador_Ponto user in users)
                        emails.Add(new UserCoordinates(user.Utilizadoremail, new Point(user.Pontolatitude, user.Pontolongitude)));
                }
                return emails;
            }
        }

        public ICollection<UserPoint> Values
        {
            get
            {
                List<UserPoint> users = new List<UserPoint>();
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var points = from user in db.Utilizador_Pontos select user;

                    foreach (Utilizador_Ponto aux in points)
                        users.Add(new UserPoint(new Point(aux.Pontolatitude, aux.Pontolongitude), aux.Utilizadoremail, 
                        aux.nome_do_local, aux.descricao, aux.primeiro, null));
                }
                return users;
            }
        }

        public void Add(KeyValuePair<UserCoordinates, UserPoint> item)
        {
            this.Add(item.Key, item.Value);
        }

        public void Add(UserCoordinates key, UserPoint value)
        {
            if (key == null)
                throw new ArgumentNullException("Key is null");
            else if (this.IsReadOnly == true)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var points = from point
                                 in db.Utilizador_Pontos
                                 where point.Utilizadoremail.Equals(key.Email) &&
                                       point.Pontolatitude == key.Coordinates.Latitude &&
                                       point.Pontolongitude == key.Coordinates.Longitude
                                 select point;

                    if (points.Any())
                        throw new ArgumentException("An Element with the same Key already exists");
                    else
                    {
                        Utilizador_Ponto aux = new Utilizador_Ponto();

                        aux.Utilizadoremail = key.Email;
                        aux.Pontolatitude = key.Coordinates.Latitude;
                        aux.Pontolongitude = key.Coordinates.Longitude;
                        aux.nome_do_local = value.Name;
                        aux.descricao = value.Description;
                        aux.primeiro = value.First;

                        db.Utilizador_Pontos.InsertOnSubmit(aux);

                        try
                        {
                            db.SubmitChanges();
                        }
                        catch (System.Data.SqlClient.SqlException e) { }
                    }
                }
            }
        }

        public void Clear()
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                    db.ExecuteCommand("DELETE FROM Utilizador_Ponto");
            }
        }

        public bool Contains(KeyValuePair<UserCoordinates, UserPoint> item)
        {
            bool exists;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from point
                            in db.Utilizador_Pontos
                            where point.Utilizadoremail.Equals(item.Key.Email) &&
                                  point.Pontolatitude == item.Key.Coordinates.Latitude &&
                                  point.Pontolongitude == item.Key.Coordinates.Longitude
                            select point;

                if (users.Any())
                {
                    Utilizador_Ponto aux = users.First();
                    UserCoordinates usr = new UserCoordinates(aux.Utilizadoremail, new Point(aux.Pontolatitude, aux.Pontolongitude));

                    if (usr.Equals(item.Value))
                        exists = true;
                    else
                        exists = false;
                }
                else
                    exists = false;
            }
            return exists;
        }

        public bool ContainsKey(UserCoordinates key)
        {
            bool exists;

            if (key == null)
                throw new ArgumentNullException("Key is null");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from point
                                in db.Utilizador_Pontos
                                where point.Utilizadoremail.Equals(key.Email) &&
                                      point.Pontolatitude == key.Coordinates.Latitude &&
                                      point.Pontolongitude == key.Coordinates.Longitude
                                select point;

                    if (users.Any())
                        exists = true;
                    else
                        exists = false;
                }
            }
            return exists;
        }

        public void CopyTo(KeyValuePair<UserCoordinates, UserPoint>[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("Array is null");
            else if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException("Array Index is less than 0");
            else if ((array.Length - arrayIndex) < this.Count)
                throw new ArgumentException("The number of elements in the source is greater" +
                    "than the available space from arrayIndex to the end of the destination array");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from user in db.Utilizador_Pontos select user;

                    foreach (Utilizador_Ponto aux in users)
                        array[arrayIndex++] = new KeyValuePair<UserCoordinates, UserPoint>(
                        new UserCoordinates(aux.Utilizadoremail, new Point(aux.Pontolatitude, aux.Pontolongitude)),
                        new UserPoint(new Point(aux.Pontolatitude, aux.Pontolongitude), aux.Utilizadoremail, 
                        aux.nome_do_local, aux.descricao, aux.primeiro, null));
                }
            }
        }

        public IEnumerator<KeyValuePair<UserCoordinates, UserPoint>> GetEnumerator()
        {
            IEnumerator<KeyValuePair<UserCoordinates, UserPoint>> res;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                List<KeyValuePair<UserCoordinates, UserPoint>> aux = new List<KeyValuePair<UserCoordinates, UserPoint>>();
                var users = from user in db.Utilizador_Pontos select user;

                foreach (Utilizador_Ponto user in users)
                    aux.Add(new KeyValuePair<UserCoordinates, UserPoint>(
                        new UserCoordinates(user.Utilizadoremail, new Point(user.Pontolatitude, user.Pontolongitude)),
                        new UserPoint(new Point(user.Pontolatitude, user.Pontolongitude), user.Utilizadoremail, 
                        user.nome_do_local, user.descricao, user.primeiro, null)));

                res = aux.GetEnumerator();
            }
            return res;
        }

        public bool Remove(KeyValuePair<UserCoordinates, UserPoint> item)
        {
            return this.Remove(item.Key);
        }

        public bool Remove(UserCoordinates key)
        {
            bool aux;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from point
                            in db.Utilizador_Pontos
                            where point.Utilizadoremail.Equals(key.Email) &&
                                  point.Pontolatitude == key.Coordinates.Latitude &&
                                  point.Pontolongitude == key.Coordinates.Longitude
                            select point;

                if (!users.Any())
                    aux = false;
                else
                {
                    foreach (Utilizador_Ponto user in users)
                        db.Utilizador_Pontos.DeleteOnSubmit(user);
                    db.SubmitChanges();
                    aux = true;
                }
            }

            return aux;
        }

        public bool TryGetValue(UserCoordinates key, out UserPoint value)
        {
            if (key == null)
                throw new ArgumentNullException("Key is null");
            else
            {
                bool aux;

                try
                {
                    value = this[key];
                    aux = true;
                }
                catch (KeyNotFoundException e)
                {
                    value = null;
                    aux = false;
                }
                return aux;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public List<UserPoint> Points_From_User(string email)
        {
            List<UserPoint> aux = new List<UserPoint>();
            var enumerator = this.GetEnumerator();

            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Key.Email.Equals(email))
                    aux.Add(enumerator.Current.Value);
            }
            return aux;
        }
    }
}
