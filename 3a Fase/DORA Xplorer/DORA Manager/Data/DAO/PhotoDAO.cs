﻿using Business;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Data.DAO
{
    public class PhotoDAO : IDictionary<UserPoint, List<Photo>>
    {
        public List<Photo> this[UserPoint key]
        {
            get
            {
                if (key == null)
                    throw new ArgumentNullException("Key is null");
                else
                {
                    List<Photo> aux = new List<Photo>();

                    using (DORA_DBDataContext db = new DORA_DBDataContext())
                    {
                        var images = from photo
                                     in db.Fotos
                                     where key.Email.Equals(photo.email) &&
                                           key.Latitude == photo.latitude &&
                                           key.Longitude == photo.longitude
                                     select photo;

                        if (!images.Any())
                            throw new KeyNotFoundException("Key not found");
                        else
                        {
                            foreach (Foto p in images)
                                aux.Add(new Photo("Point (" + key.Latitude + " , " + key.Longitude + ")", p.foto1.ToArray()));
                            return aux;
                        }
                    }
                }
            }

            set
            {
                if (key == null)
                    throw new ArgumentNullException("Key is null");
                else if (this.IsReadOnly == true)
                    throw new NotSupportedException("The Table is read-only");
                else
                {
                    using (DORA_DBDataContext db = new DORA_DBDataContext())
                    {
                        var images = from photo
                                     in db.Fotos
                                     where photo.email.Equals(key.Name) &&
                                           photo.latitude == key.Latitude &&
                                           photo.longitude == key.Longitude
                                     select photo;

                        if (!images.Any())
                            throw new KeyNotFoundException("Key not found");
                        else
                        {
                            this.Remove(key);
                            this.Add(key, value);
                        }
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                int aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Fotos.Count();
                }
                return aux;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                bool aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Fotos.IsReadOnly;
                }
                return aux;
            }
        }

        public ICollection<UserPoint> Keys
        {
            get
            {
                HashSet<UserPoint> emails = new HashSet<UserPoint>();
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from photo
                                in db.Fotos
                                select photo;
                    foreach (Foto p in users)
                        emails.Add(new UserPoint(new Business.Point(p.latitude, p.longitude), p.email, "", "", null));
                }
                return emails;
            }
        }

        public ICollection<List<Photo>> Values
        {
            get
            {
                List<List<Photo>> photos = new List<List<Photo>>();
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    foreach (UserPoint k in Keys)
                        photos.Add(this[k]);
                }
                return photos;
            }
        }

        public void Add(KeyValuePair<UserPoint, List<Photo>> item)
        {
            this.Add(item.Key, item.Value);
        }

        public void Add(UserPoint key, List<Photo> value)
        {
            if (key == null)
                throw new ArgumentNullException("Key is null");
            else if (this.IsReadOnly == true)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from photo
                                in db.Fotos
                                where photo.email.Equals(key.Name) &&
                                      photo.latitude == key.Latitude &&
                                      photo.longitude == key.Longitude
                                select photo;

                    if (users.Any())
                        throw new ArgumentException("An Element with the same Key already exists");
                    else
                    {
                        foreach (Photo p in value)
                        {
                            Foto aux = new Foto();
                            aux.email = key.Email;
                            aux.latitude = key.Latitude;
                            aux.longitude = key.Longitude;
                            aux.foto1 = ImageSourceToBytes(new JpegBitmapEncoder(), p.Image);
                            
                            db.Fotos.InsertOnSubmit(aux);
                        }
                        db.SubmitChanges();
                    }
                }
            }
        }

        public byte[] ImageSourceToBytes(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }
            return bytes;
        }

        public void Clear()
        {
            if (IsReadOnly)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                    db.ExecuteCommand("DELETE FROM Foto");
            }
        }

        public bool Contains(KeyValuePair<UserPoint, List<Photo>> item)
        {
            bool exists = false;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from photo
                            in db.Fotos
                            where photo.email.Equals(item.Key.Name) &&
                                  photo.latitude == item.Key.Latitude &&
                                  photo.longitude == item.Key.Longitude
                            select photo;

                if (users.Any())
                    exists = true;
                else
                    exists = false;
            }
            return exists;
        }

        public bool ContainsKey(UserPoint key)
        {
            bool exists;

            if (key == null)
                throw new ArgumentNullException("Key is null");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from photo
                                in db.Fotos
                                where photo.email.Equals(key.Name) &&
                                      photo.latitude == key.Latitude &&
                                      photo.longitude == key.Longitude
                                select photo;

                    if (users.Any())
                        exists = true;
                    else
                        exists = false;
                }
            }
            return exists;
        }

        public void CopyTo(KeyValuePair<UserPoint, List<Photo>>[] array, int arrayIndex)
        {
            throw new NotSupportedException();
        }

        public IEnumerator<KeyValuePair<UserPoint, List<Photo>>> GetEnumerator()
        {
            throw new NotSupportedException();
        }

        public bool Remove(KeyValuePair<UserPoint, List<Photo>> item)
        {
            return this.Remove(item.Key);
        }

        public bool Remove(UserPoint key)
        {
            bool aux;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from photo
                                in db.Fotos
                            where photo.email.Equals(key.Name) &&
                                  photo.latitude == key.Latitude &&
                                  photo.longitude == key.Longitude
                            select photo;

                if (!users.Any())
                    aux = false;
                else
                {
                    foreach (Foto user in users)
                        db.Fotos.DeleteOnSubmit(user);
                    db.SubmitChanges();
                    aux = true;
                }
            }
            return aux;
        }

        public bool TryGetValue(UserPoint key, out List<Photo> value)
        {
            if (key == null)
                throw new ArgumentNullException("Key is null");
            else
            {
                bool aux;

                try
                {
                    value = this[key];
                    aux = true;
                }
                catch (KeyNotFoundException e)
                {
                    value = new List<Photo>();
                    aux = false;
                }
                return aux;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
