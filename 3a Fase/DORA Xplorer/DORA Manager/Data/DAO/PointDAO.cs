﻿using Business;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections;

namespace Data.DAO
{
    public class PointDAO : ISet<Point>
    {
        public int Count
        {
            get
            {
                int aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Pontos.Count();
                }
                return aux;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                bool aux;

                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    aux = db.Pontos.IsReadOnly;
                }
                return aux;
            }
        }

        public bool Add(Point item)
        {
            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var points = from point
                             in db.Pontos
                             where point.latitude == item.Latitude &&
                                   point.longitude == item.Longitude
                             select point;

                if (points.Any())
                    return false;
                else
                {
                    Ponto aux = new Ponto();
                    
                    aux.latitude = item.Latitude;
                    aux.longitude = item.Longitude;

                    db.Pontos.InsertOnSubmit(aux);
                    db.SubmitChanges();
                    return true;
                }
            }
        }

        public void Clear()
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("The Table is read-only");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                    db.ExecuteCommand("DELETE FROM Ponto");
            }
        }

        public bool Contains(Point item)
        {
            bool exists;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from point
                            in db.Pontos
                            where point.latitude == item.Latitude &&
                                  point.longitude == item.Longitude
                            select point;

                if (users.Any())
                {
                    Ponto aux = users.First();

                    if (aux.Equals(item))
                        exists = true;
                    else
                        exists = false;
                }
                else
                    exists = false;
            }
            return exists;
        }

        public void CopyTo(Point[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("Array is null");
            else if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException("Array Index is less than 0");
            else if ((array.Length - arrayIndex) < this.Count)
                throw new ArgumentException("The number of elements in the source is greater" +
                    "than the available space from arrayIndex to the end of the destination array");
            else
            {
                using (DORA_DBDataContext db = new DORA_DBDataContext())
                {
                    var users = from user in db.Pontos select user;

                    foreach (Ponto aux in users)
                        array[arrayIndex++] = new Point(aux.latitude, aux.longitude);
                }
            }
        }

        public void ExceptWith(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<Point> GetEnumerator()
        {
            IEnumerator<Point> res;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                List<Point> list = new List<Point>();
                var users = from user in db.Pontos select user;

                foreach (Ponto aux in users)
                    list.Add(new Point(aux.latitude, aux.longitude));

                res = list.GetEnumerator();
            }
            return res;
        }

        public void IntersectWith(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public bool IsProperSubsetOf(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public bool IsProperSupersetOf(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public bool IsSubsetOf(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public bool IsSupersetOf(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public bool Overlaps(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public bool Remove(Point item)
        {
            bool aux;

            using (DORA_DBDataContext db = new DORA_DBDataContext())
            {
                var users = from point
                            in db.Pontos
                            where point.latitude == item.Latitude &&
                                  point.longitude == item.Longitude
                            select point;

                if (!users.Any())
                    aux = false;
                else
                {
                    foreach (Ponto user in users)
                        db.Pontos.DeleteOnSubmit(user);
                    db.SubmitChanges();
                    aux = true;
                }
            }
            return aux;
        }

        public bool SetEquals(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public void SymmetricExceptWith(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        public void UnionWith(IEnumerable<Point> other)
        {
            throw new NotImplementedException();
        }

        void ICollection<Point>.Add(Point item)
        {
            this.Add(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
