﻿using BingMapsRESTService.Common.JSON;
using Business;
using Data.DAO;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;

namespace DORA_Manager.Presentation
{
    /// <summary>
    /// Interaction logic for Map.xaml
    /// </summary>
    public partial class MapPage : Page
    {
        int pin_nr = 1;
        bool first = true;
        public bool first_location = true;
        Pushpin currentLocation;
        Microsoft.Maps.MapControl.WPF.Location last_pin;
        Business.DORA_Manager man = new Business.DORA_Manager();
        List<Microsoft.Maps.MapControl.WPF.Location> pins = new List<Microsoft.Maps.MapControl.WPF.Location>();

        public MapPage()
        {
            InitializeComponent();
        }


        // Push a list of points

        public List<Pushpin> Push_Points(List<Business.Point> points, bool aux, string user)
        {
            List<Pushpin> au = new List<Pushpin>();

            foreach (Business.Point p in points)
            {
                // Custom pushpin
                Pushpin pin = new Pushpin();
                pin.Cursor = Cursors.Hand;
                pin.Location = new Microsoft.Maps.MapControl.WPF.Location(p.Latitude, p.Longitude);

                Ellipse myEllipse = new Ellipse();
                SolidColorBrush mySolidColorBrush;

                List<Activity> acts = man.Get_Activities_Of_User(user);
                List<string> refs = new List<string>();

                foreach (Activity a in acts)
                    if (a.Email.Equals(user))
                        refs.Add(a.Reference);

                List<ActivityPoint> all_points = new List<ActivityPoint>();
                foreach(ActivityPoint f in new ActivityPointDAO())
                {
                    all_points.Add(f);
                }

                if (p.Discovered(man, all_points))
                {
                    mySolidColorBrush = new SolidColorBrush();

                    if (p.Discovered_By_User(man, refs, all_points, user))
                    {
                        mySolidColorBrush.Color = Color.FromRgb(0, 255, 0);
                        ToolTipService.SetToolTip(pin, "[Coordinates]\n >Latitude: " + p.Latitude + "\n >Longitude: " + p.Longitude + "\n[User Input]\n >Name: " + man.Get_User_Point_Name(user, p) + "\n >Description: " + man.Get_User_Point_Description(user, p));
                    }
                    else
                    {
                        mySolidColorBrush.Color = Color.FromRgb(255, 0, 0);
                        ToolTipService.SetToolTip(pin, "????");
                    }
                }
                else
                {
                    mySolidColorBrush = new SolidColorBrush();
                    mySolidColorBrush.Color = Color.FromRgb(56, 91, 226);
                    ToolTipService.SetToolTip(pin, "????");
                }

                // Describes the brush's color using RGB values. 
                // Each value has a range of 0-255.
                myEllipse.Fill = mySolidColorBrush;

                // Set the width and height of the Ellipse.
                myEllipse.Width = 20;
                myEllipse.Height = 20;

                pin.Content = myEllipse;

                // Adds the pushpin to the map.
                myMap.Children.Add(pin);
                au.Add(pin);

                if (aux)
                {
                    List<Pushpin> aux_pts = GetNearbyPOI(pin.Location.Latitude, pin.Location.Longitude, user);

                    foreach (Pushpin pi in aux_pts)
                        au.Add(pi);
                }
            }
            return au;
        }

        public void Resize(int width, int height)
        {
            Grid.Width = width;
            Grid.Height = height;
        }


        // Current Location

        public void GetCurrentLocation()
        {
            GeoCoordinateWatcher watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default);
            watcher.PositionChanged += _watcher_PositionChanged;
            watcher.Start(); //started watcher
            GeoCoordinate coord = watcher.Position.Location;
            if (!watcher.Position.Location.IsUnknown)
            {
                double lat = coord.Latitude; //latitude
                double lng = coord.Longitude;  //logitude
                
                Microsoft.Maps.MapControl.WPF.Location pinLocation = new Microsoft.Maps.MapControl.WPF.Location(lat, lng);
                currentLocation = new Pushpin();
                currentLocation.Location = pinLocation;
            }
        }
        
        private void _watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            Remove_Pin(currentLocation);
            Microsoft.Maps.MapControl.WPF.Location pinLocation = 
                new Microsoft.Maps.MapControl.WPF.Location(e.Position.Location.Latitude, e.Position.Location.Longitude);
            Push_Location(pinLocation);

            if (first_location)
            {
                Center_Map(pinLocation, 17.0);
                first_location = false;
            }
        }

        public Business.Point PushPin_Click(object sender, MouseButtonEventArgs e)
        {
            // Disables the default mouse double-click action.
            e.Handled = true;
            // Determin the location to place the pushpin at on the map.
            // Get the mouse click coordinates
            Pushpin pin = sender as Pushpin;
            // Convert the mouse coordinates to a locatoin on the map
            Microsoft.Maps.MapControl.WPF.Location pinLocation = new Microsoft.Maps.MapControl.WPF.Location(pin.Location.Latitude, pin.Location.Longitude);

            if (first)
            {
                first = false;
                last_pin = pinLocation;
            }
            else
            {
                string pin1 = string.Format("{0},{1}",
                    last_pin.Latitude.ToString(CultureInfo.InvariantCulture),
                    last_pin.Longitude.ToString(CultureInfo.InvariantCulture));
                string pin2 = string.Format("{0},{1}",
                    pinLocation.Latitude.ToString(CultureInfo.InvariantCulture),
                    pinLocation.Longitude.ToString(CultureInfo.InvariantCulture));
                last_pin = pinLocation;
                GetRoute(pin1, pin2);
            }
            return new Business.Point(pinLocation.Latitude, pinLocation.Longitude);
        }


        // Pushpins

        public Business.Point MouseDoubleClick(object sender, MouseButtonEventArgs e, string label, int number)
        {
            pin_nr = number;
            // Disables the default mouse double-click action.
            e.Handled = true;
            // Determin the location to place the pushpin at on the map.
            // Get the mouse click coordinates
            System.Windows.Point mousePosition = e.GetPosition(this);
            // Convert the mouse coordinates to a locatoin on the map
            Microsoft.Maps.MapControl.WPF.Location pinLocation = myMap.ViewportPointToLocation(mousePosition);
            // The pushpin to add to the map.
            Push_Pin(pinLocation, label, number);

            if (first)
            {
                first = false;
                last_pin = pinLocation;
            }
            else
            {
                string pin1 = string.Format("{0},{1}",
                    last_pin.Latitude.ToString(CultureInfo.InvariantCulture),
                    last_pin.Longitude.ToString(CultureInfo.InvariantCulture));
                string pin2 = string.Format("{0},{1}",
                    pinLocation.Latitude.ToString(CultureInfo.InvariantCulture),
                    pinLocation.Longitude.ToString(CultureInfo.InvariantCulture));
                GetRoute(pin1, pin2);
            }

            return new Business.Point(pinLocation.Latitude, pinLocation.Longitude);
        }


        // Point to Route

        public void Route_Point(Business.Point a, string label, int number)
        {
            Microsoft.Maps.MapControl.WPF.Location pinLocation = new Microsoft.Maps.MapControl.WPF.Location(a.Latitude, a.Longitude);

            // The pushpin to add to the map.
            Push_Pin(pinLocation,label,number);

            if (first)
            {
                first = false;
                last_pin = pinLocation;
            }
            else
            {
                string pin1 = string.Format("{0},{1}",
                    last_pin.Latitude.ToString(CultureInfo.InvariantCulture),
                    last_pin.Longitude.ToString(CultureInfo.InvariantCulture));
                string pin2 = string.Format("{0},{1}",
                    pinLocation.Latitude.ToString(CultureInfo.InvariantCulture),
                    pinLocation.Longitude.ToString(CultureInfo.InvariantCulture));
                GetRoute(pin1, pin2);
            }
        }


        // Push House

        public List<Pushpin> Push_House(Microsoft.Maps.MapControl.WPF.Location pinLocation, string label, string user, bool aux)
        {
            myMap.Children.Remove(currentLocation);

            // Custom pushpin
            Pushpin pin = new Pushpin();
            pin.Cursor = Cursors.Hand;
            pin.Location = pinLocation;
            ToolTipService.SetToolTip(pin, label);

            Ellipse myEllipse = new Ellipse();

            SolidColorBrush mySolidColorBrush = new SolidColorBrush();

            // Describes the brush's color using RGB values. 
            // Each value has a range of 0-255.
            mySolidColorBrush.Color = Color.FromRgb(0, 0, 0);
            myEllipse.Fill = mySolidColorBrush;

            // Set the width and height of the Ellipse.
            myEllipse.Width = 20;
            myEllipse.Height = 20;

            pin.Content = myEllipse;
            currentLocation = pin;

            // Adds the pushpin to the map.
            myMap.Children.Add(pin);

            if (aux)
                return GetNearbyPOI(pin.Location.Latitude, pin.Location.Longitude, user);
            else
                return new List<Pushpin>();
        }

        public void Push_Location(Microsoft.Maps.MapControl.WPF.Location pinLocation)
        {
            myMap.Children.Remove(currentLocation);
            Pushpin pin = new Pushpin();
            pin.Location = pinLocation;
            currentLocation = pin;
            // Adds the pushpin to the map.
            myMap.Children.Add(pin);
        }

        public void Push_Pin(Microsoft.Maps.MapControl.WPF.Location pinLocation, string label, int number)
        {
            Pushpin pin = new Pushpin();
            pin.Cursor = Cursors.Hand;
            pin.Location = pinLocation;
            // pin.Content = number;
            ToolTipService.SetToolTip(pin, label);

            // Adds the pushpin to the map.
            myMap.Children.Add(pin);
            pins.Add(pinLocation);
        }

        public void Remove_Pin(Pushpin pin)
        {
            // Removes the pushpin to the map.
            myMap.Children.Remove(pin);
        }

        public void Center_Map(Microsoft.Maps.MapControl.WPF.Location pinLocation, double zoom)
        {
            myMap.Center = pinLocation;
            myMap.ZoomLevel = zoom;
        }


        // Bing Maps REST API - Routes

        private void Route(string start, string end, string key, Action<Response> callback)
        {
            Uri requestURI = new Uri(
                string.Format("http://dev.virtualearth.net/REST/V1/Routes/Walking?wp.0={0}&wp.1={1}&rpo=Points&key={2}", 
                Uri.EscapeDataString(start), Uri.EscapeDataString(end), key));
            GetResponse(requestURI, callback);
        }

        private void GetResponse(Uri uri, Action<Response> callback)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Response));

                        if (callback != null)
                        {
                            callback(ser.ReadObject(stream) as Response);
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void GetKey(Action<string> callback)
        {
            if (callback != null)
            {
                myMap.CredentialsProvider.GetCredentials((c) =>
                {
                    callback("AhJG0Cv6ZQAdinukVOuB3JWPHpdyr9NdFkAo0HQHIEYweXUZlw44D56_o9tTaxn2");
                });
            }
        }

        private void GetRoute(string from, string to)
        {
            if (!string.IsNullOrWhiteSpace(from))
            {
                if (!string.IsNullOrWhiteSpace(to))
                {
                    GetKey((c) =>
                    {
                        Route(from, to, c, (r) =>
                        {
                            if (r != null &&
                                r.ResourceSets != null &&
                                r.ResourceSets.Length > 0 &&
                                r.ResourceSets[0].Resources != null &&
                                r.ResourceSets[0].Resources.Length > 0)
                            {
                                Route route = r.ResourceSets[0].Resources[0] as Route;

                                double[][] routePath = route.RoutePath.Line.Coordinates;
                                LocationCollection locs = new LocationCollection();

                                for (int i = 0; i < routePath.Length; i++)
                                {
                                    if (routePath[i].Length >= 2)
                                    {
                                        locs.Add(new Microsoft.Maps.MapControl.WPF.Location(routePath[i][0], routePath[i][1]));
                                    }
                                }

                                MapPolyline routeLine = new MapPolyline()
                                {
                                    Locations = locs,
                                    Stroke = new SolidColorBrush(Color.FromRgb(58, 91, 226)),
                                    StrokeThickness = 5
                                };

                                myMap.Children.Add(routeLine);

                                myMap.SetView(locs, new Thickness(30), 0);
                            }
                            else
                            {
                                MessageBox.Show("No Results found.");
                            }
                        });
                    });
                }
                else
                {
                    MessageBox.Show("Invalid End location.");
                }
            }
            else
            {
                MessageBox.Show("Invalid Start location.");
            }
        }


        // Bing Maps REST Api - Geocode

        public GeoCoordinate Geocode(string location)
        {
            string request = CreateRequest(location);
            Response answer = MakeRequest(request);
            GeoCoordinate coords = GetCoordinates(answer);
            return coords;
        }

        public string Geocode(double latitude, double longitude)
        {
            string location = string.Format("{0},{1}",
                latitude.ToString(CultureInfo.InvariantCulture),
                longitude.ToString(CultureInfo.InvariantCulture));
            string request = CreateRequest(location);
            Response answer = MakeRequest(request);
            string address = GetFullAddress(answer);
            return address;
        }

        private string CreateRequest(string queryString)
        {
            string UrlRequest = "http://dev.virtualearth.net/REST/v1/Locations/" +
                                           queryString +
                                           "?key=AhJG0Cv6ZQAdinukVOuB3JWPHpdyr9NdFkAo0HQHIEYweXUZlw44D56_o9tTaxn2";
            return (UrlRequest);
        }

        private Response MakeRequest(string requestUrl)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception(String.Format(
                        "o servidor reportou um erro (HTTP {0}: {1}).",
                        response.StatusCode,
                        response.StatusDescription));
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                    Response jsonResponse = objResponse as Response;
                    return jsonResponse;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return null;
            }
        }

        private string GetFullAddress(Response locationsResponse)
        {
            if (locationsResponse != null)
            {
                if (locationsResponse.ResourceSets.Length > 0)
                {
                    if (locationsResponse.ResourceSets[0].Resources.Length > 0)
                    {
                        BingMapsRESTService.Common.JSON.Location location =
                            (BingMapsRESTService.Common.JSON.Location)locationsResponse.ResourceSets[0].Resources[0];
                        return location.Address.FormattedAddress;
                    }
                }
            }
            return "";
        }

        private GeoCoordinate GetCoordinates(Response locationsResponse)
        {
            if (locationsResponse != null)
            {
                if (locationsResponse.ResourceSets.Length > 0)
                {
                    if (locationsResponse.ResourceSets[0].Resources.Length > 0)
                    {
                        BingMapsRESTService.Common.JSON.Location location =
                            (BingMapsRESTService.Common.JSON.Location)locationsResponse.ResourceSets[0].Resources[0];
                        return new GeoCoordinate(location.GeocodePoints[0].Coordinates[0],
                                                 location.GeocodePoints[0].Coordinates[1]);
                    }
                }
            }
            return new GeoCoordinate(0.0, 0.0);
        }


        // Wikipedia Nearby API - Points of Interest

        private List<Pushpin> GetNearbyPOI(double latitude, double longitude, string user)
        {
            double lat, lon;
            List<Business.Point> aux = new List<Business.Point>();

            string url = string.Format("https://en.wikipedia.org/w/api.php?action=query&list=geosearch&gscoord={0}|{1}&gsradius=10000&gslimit=30&format=xml",
                latitude.ToString(CultureInfo.InvariantCulture),
                longitude.ToString(CultureInfo.InvariantCulture));

            WebClient client = new WebClient();
            string xml_text = client.DownloadString(url);
            string aux_value;

            using (XmlReader reader = XmlReader.Create(new StringReader(xml_text)))
            {
                while (reader.ReadToFollowing("gs"))
                {
                    reader.MoveToAttribute("lat");
                    aux_value = reader.Value;
                    aux_value = aux_value.Trim(new char[] { '\"' });
                    aux_value = aux_value.Replace('.', ',');
                    lat = Convert.ToDouble(aux_value);

                    reader.MoveToAttribute("lon");
                    aux_value = reader.Value;
                    aux_value = aux_value.Trim(new char[] { '\"' });
                    aux_value = aux_value.Replace('.', ',');
                    lon = Convert.ToDouble(aux_value);

                    aux.Add(new Business.Point(lat, lon));
                }
            }
            return Push_Points(aux, false, user);
        }
    }
}
