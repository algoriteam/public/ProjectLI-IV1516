﻿using Presentation;
using System.Windows;
using System.Windows.Controls;

namespace DORA_Manager.Presentation
{
    /// <summary>
    /// Interaction logic for Map.xaml
    /// </summary>
    public partial class Exit : Page
    {
        MenuWindow my_window;

        public Exit(MenuWindow window)
        {
            InitializeComponent();
            my_window = window;
        }

        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            my_window.Yes_Exit();
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            my_window.No_Exit();
        }
    }
}
