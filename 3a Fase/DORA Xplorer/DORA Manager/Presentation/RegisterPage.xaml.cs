﻿using Microsoft.Maps.MapControl.WPF;
using System.Device.Location;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace DORA_Manager.Presentation
{
    /// <summary>
    /// Interaction logic for RegisterPage.xaml
    /// </summary>
    public partial class RegisterPage : Page
    {
        string name;
        string telephone;
        string facebook;
        string address;
        private MapPage my_map;
        private Pushpin my_house;

        public RegisterPage()
        {
            InitializeComponent();
            name = "";
            telephone = "";
            facebook = "";
            address = "";
            my_map = new MapPage();
            my_house = new Pushpin();
            my_map.Resize(477, 170);

            InsideFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
            InsideFrame.Navigate(my_map);
        }

        private void Search_Button_Click(object sender, RoutedEventArgs e)
        {
            address = Address.Text;
            if (!address.Equals(""))
            {
                GeoCoordinate aux = my_map.Geocode(address);
                
                Location pinLocation = new Location(aux.Latitude, aux.Longitude);
                my_map.Push_Location(pinLocation);
                my_map.Center_Map(pinLocation, 17.0);
            }
        }
    }
}
