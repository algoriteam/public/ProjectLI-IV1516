﻿using Business;
using DORA_Manager.Presentation;
using MahApps.Metro.Controls;
using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace Presentation
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : MetroWindow
    {
        private Business.DORA_Manager man;
        private User current_user;
        private MapPage my_map;
        private Location my_house;
        private RouteWindow my_route;

        private bool first_point, building_route;
        private int pin_number;
        

        public MenuWindow(Business.DORA_Manager dora, User user)
        {
            InitializeComponent();

            Application.Current.Exit += Current_Exit;

            man = dora;
            current_user = user;
            Username.Content = user.Email;
            my_map = new MapPage();
            my_map.Resize(625,515);
            Avatar.Source = current_user.Image;

            my_route = new RouteWindow(this, man, current_user);

            InsideFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
            InsideFrame.Navigate(my_map);

            List<Pushpin> aux = my_map.Push_Points(man.Get_Points_Of_User(current_user.Email), true, user.Email);

            foreach (Pushpin p in aux)
                p.PreviewMouseDown += new MouseButtonEventHandler(PushPin_Click);

            // COLOCAR UMA CASA NA MORADA DO USER
            my_house = new Location(user.Latitude, user.Longitude);

            List<Pushpin> aux_pts = my_map.Push_House(my_house, "[Coordinates]\n >Latitude: "+user.Latitude+ "\n >Longitude: " + user.Longitude + "\n[Address]\n >Street: " + user.Street + "\n >Locality: " + user.Locality, user.Email, true);

            foreach (Pushpin p in aux_pts)
                p.PreviewMouseDown += new MouseButtonEventHandler(PushPin_Click);

            my_map.Center_Map(my_house, 17.0);

            first_point = true;
            building_route = false;
            pin_number = 0;
        }

        public void SetUser(User u)
        {
            current_user = u;
            Avatar.Source = current_user.Image;
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            man.Logout(current_user.Email);
        }

        public void PushPin_Click(object sender, MouseButtonEventArgs e)
        {
            if (building_route)
            {
                pin_number++;
                Business.Point mousePosition = my_map.PushPin_Click(sender, e);

                if (mousePosition != null)
                {
                    if (first_point)
                    {
                        my_route.first_point(mousePosition);
                        first_point = false;
                    }
                    else
                        my_route.new_point(mousePosition);
                }
            }
        }

        private void Avatar_MouseEnter(object sender, MouseEventArgs e)
        {
            Avatar_Background.Source = new BitmapImage(new Uri("/Resources/LOGOUT_AVATAR.png", UriKind.Relative));
            Avatar.Source = null;
        }

        private void Avatar_MouseLeave(object sender, MouseEventArgs e)
        {
            Avatar.Source = current_user.Image;
        }

        private void Exit_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Exit.Visibility = Visibility.Hidden;
            ExitOpen.Visibility = Visibility.Visible;

            HistoryOpen.Visibility = Visibility.Hidden;
            ProfileOpen.Visibility = Visibility.Hidden;
            SyncOpen.Visibility = Visibility.Hidden;
            RouteOpen.Visibility = Visibility.Hidden;

            History.Visibility = Visibility.Visible;
            Profile.Visibility = Visibility.Visible;
            Sync.Visibility = Visibility.Visible;
            Route.Visibility = Visibility.Visible;

            BLACK.Visibility = Visibility.Hidden;
            RED.Visibility = Visibility.Hidden;
            GREEN.Visibility = Visibility.Hidden;
            BLUE.Visibility = Visibility.Hidden;

            Black_Text.Visibility = Visibility.Hidden;
            Red_Text.Visibility = Visibility.Hidden;
            Green_Text.Visibility = Visibility.Hidden;
            Blue_Text.Visibility = Visibility.Hidden;

            Scroller.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;

            InsideFrame.Navigate(new Exit(this));
            CenterWindowOnScreen();
            my_route.Close();
        }

        private void Route_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Route.Visibility = Visibility.Hidden;
            RouteOpen.Visibility = Visibility.Visible;

            HistoryOpen.Visibility = Visibility.Hidden;
            ProfileOpen.Visibility = Visibility.Hidden;
            SyncOpen.Visibility = Visibility.Hidden;
            ExitOpen.Visibility = Visibility.Hidden;

            History.Visibility = Visibility.Visible;
            Profile.Visibility = Visibility.Visible;
            Sync.Visibility = Visibility.Visible;
            Exit.Visibility = Visibility.Visible;

            BLACK.Visibility = Visibility.Visible;
            RED.Visibility = Visibility.Visible;
            GREEN.Visibility = Visibility.Visible;
            BLUE.Visibility = Visibility.Visible;

            Black_Text.Visibility = Visibility.Visible;
            Red_Text.Visibility = Visibility.Visible;
            Green_Text.Visibility = Visibility.Visible;
            Blue_Text.Visibility = Visibility.Visible;

            Scroller.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;

            my_map = new MapPage();
            InsideFrame.Navigate(my_map);
            List<Pushpin> aux = my_map.Push_Points(man.Get_Points_Of_User(current_user.Email), true, current_user.Email);

            foreach (Pushpin p in aux)
                p.PreviewMouseDown += new MouseButtonEventHandler(PushPin_Click);

            // COLOCAR UMA CASA NA MORADA DO USER
            my_house = new Location(current_user.Latitude, current_user.Longitude);
            List<Pushpin> aux_pts = my_map.Push_House(my_house, "[Address]\n >Street: " + current_user.Street + "\n >Locality: " + current_user.Locality, current_user.Email, true);

            foreach (Pushpin p in aux_pts)
                p.PreviewMouseDown += new MouseButtonEventHandler(PushPin_Click);

            my_map.Center_Map(my_house, 17.0);

            building_route = true;
            pin_number = 0;

            CenterWindowOnScreen();
            Left -= my_route.Width / 2;
            my_route.Close();

            // POSICIONAR?
            my_route = new RouteWindow(this, man, current_user);
            my_route.Show();

            my_route.Left += Width / 2 + 10;
        }

        private void History_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            History.Visibility = Visibility.Hidden;
            HistoryOpen.Visibility = Visibility.Visible;

            RouteOpen.Visibility = Visibility.Hidden;
            ProfileOpen.Visibility = Visibility.Hidden;
            SyncOpen.Visibility = Visibility.Hidden;
            ExitOpen.Visibility = Visibility.Hidden;

            Route.Visibility = Visibility.Visible;
            Profile.Visibility = Visibility.Visible;
            Sync.Visibility = Visibility.Visible;
            Exit.Visibility = Visibility.Visible;

            BLACK.Visibility = Visibility.Hidden;
            RED.Visibility = Visibility.Hidden;
            GREEN.Visibility = Visibility.Hidden;
            BLUE.Visibility = Visibility.Hidden;

            Black_Text.Visibility = Visibility.Hidden;
            Red_Text.Visibility = Visibility.Hidden;
            Green_Text.Visibility = Visibility.Hidden;
            Blue_Text.Visibility = Visibility.Hidden;

            Scroller.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;

            InsideFrame.Navigate(new HistoryPage(man, current_user));
            CenterWindowOnScreen();
            my_route.Close();

            building_route = false;
        }

        private void Profile_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Profile.Visibility = Visibility.Hidden;
            ProfileOpen.Visibility = Visibility.Visible;

            RouteOpen.Visibility = Visibility.Hidden;
            HistoryOpen.Visibility = Visibility.Hidden;
            SyncOpen.Visibility = Visibility.Hidden;
            ExitOpen.Visibility = Visibility.Hidden;

            Route.Visibility = Visibility.Visible;
            History.Visibility = Visibility.Visible;
            Sync.Visibility = Visibility.Visible;
            Exit.Visibility = Visibility.Visible;

            BLACK.Visibility = Visibility.Hidden;
            RED.Visibility = Visibility.Hidden;
            GREEN.Visibility = Visibility.Hidden;
            BLUE.Visibility = Visibility.Hidden;

            Black_Text.Visibility = Visibility.Hidden;
            Red_Text.Visibility = Visibility.Hidden;
            Green_Text.Visibility = Visibility.Hidden;
            Blue_Text.Visibility = Visibility.Hidden;

            Scroller.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;

            InsideFrame.Navigate(new ProfilePage(man, current_user, this));
            CenterWindowOnScreen();
            my_route.Close();

            building_route = false;
        }

        private void Sync_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            Sync.Visibility = Visibility.Hidden;
            SyncOpen.Visibility = Visibility.Visible;

            RouteOpen.Visibility = Visibility.Hidden;
            ProfileOpen.Visibility = Visibility.Hidden;
            HistoryOpen.Visibility = Visibility.Hidden;
            ExitOpen.Visibility = Visibility.Hidden;

            Route.Visibility = Visibility.Visible;
            Profile.Visibility = Visibility.Visible;
            History.Visibility = Visibility.Visible;
            Exit.Visibility = Visibility.Visible;

            BLACK.Visibility = Visibility.Hidden;
            RED.Visibility = Visibility.Hidden;
            GREEN.Visibility = Visibility.Hidden;
            BLUE.Visibility = Visibility.Hidden;

            Black_Text.Visibility = Visibility.Hidden;
            Red_Text.Visibility = Visibility.Hidden;
            Green_Text.Visibility = Visibility.Hidden;
            Blue_Text.Visibility = Visibility.Hidden;

            Scroller.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;

            InsideFrame.Navigate(new SyncPage(man, current_user));
            CenterWindowOnScreen();
            my_route.Close();

            building_route = false;
        }

        public void Yes_Exit()
        {
            man.Logout(current_user.Email);
            Application.Current.Shutdown();
        }

        public void No_Exit()
        {
            Route.Visibility = Visibility.Hidden;
            RouteOpen.Visibility = Visibility.Visible;

            Exit.Visibility = Visibility.Visible;
            ExitOpen.Visibility = Visibility.Hidden;

            BLACK.Visibility = Visibility.Visible;
            RED.Visibility = Visibility.Visible;
            GREEN.Visibility = Visibility.Visible;
            BLUE.Visibility = Visibility.Visible;

            Black_Text.Visibility = Visibility.Visible;
            Red_Text.Visibility = Visibility.Visible;
            Green_Text.Visibility = Visibility.Visible;
            Blue_Text.Visibility = Visibility.Visible;

            InsideFrame.Navigate(my_map);

            // COLOCAR UMA CASA NA MORADA DO USER
            my_house = new Location(current_user.Latitude, current_user.Longitude);

            List<Pushpin> aux = my_map.Push_House(my_house, "[Address]\n >Street: " + current_user.Street + "\n >Locality: " + current_user.Locality, current_user.Email, true);

            foreach (Pushpin p in aux)
                p.PreviewMouseDown += new MouseButtonEventHandler(PushPin_Click);

            my_map.Center_Map(my_house, 17.0);

            my_route = new RouteWindow(this, man, current_user);
            my_route.Show();
        }

        private void Avatar_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            man.Logout(current_user.Email);
            new MainWindow().Show();
            my_route.Close();
            Close();
        }

        private void Click_Point(object sender, MouseButtonEventArgs e)
        {
            if (building_route)
            {
                pin_number++;
                Business.Point mousePosition = my_map.PushPin_Click(sender, e);

                if (mousePosition != null)
                {
                    if (first_point)
                    {
                        my_route.first_point(mousePosition);
                        first_point = false;
                    }
                    else
                        my_route.new_point(mousePosition);
                }
            }
        }

        private void InsideFrame_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (building_route) {
                pin_number++;
                Business.Point mousePosition = my_map.MouseDoubleClick(sender, e, "This is a point!\nI'm the best", pin_number);

                if (first_point)
                {
                    my_route.first_point(mousePosition);
                    first_point = false;
                }
                else my_route.new_point(mousePosition);

            }
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }
    }
}
