﻿using Business;
using Managed.Adb;
using Presentation;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System;

namespace DORA_Manager.Presentation
{
    /// <summary>
    /// Interaction logic for Map.xaml
    /// </summary>
    public partial class SyncPage : Page
    {
        private Business.DORA_Manager my_man;
        private List<Activity> lista_atividades;
        private List<Device> lista_devices;

        private User my_user;
        private string serial_device;
        private string activity_reference;

        public SyncPage(Business.DORA_Manager man, User user)
        {
            InitializeComponent();
            my_man = man;
            my_user = user;
            serial_device = "";
            activity_reference = "";

            // CONSTRUCT COMBOBOXES
            Devices_Load();
            Activities_Load();
        }

        private void Activities_Load()
        {
            // ... A List.
            lista_atividades = my_man.Get_Activities_Of_User(my_user.Email);
            List<string> data = new List<string>();

            foreach (Activity a in lista_atividades)
            {
                if(!my_man.Is_Finished(a))
                    data.Add(a.Reference);
            }

            // ... Assign the ItemsSource to the List.
            Activities.ItemsSource = data;
        }

        private void Devices_Load()
        {
            // ... A List.
            lista_devices = my_man.GetDevices();
            List<string> data = new List<string>();

            foreach (Device a in lista_devices)
                data.Add(a.Model);

            // ... Assign the ItemsSource to the List.
            Devices.ItemsSource = data;
        }

        private void Sync_Click(object sender, RoutedEventArgs e)
        {
            if (Sync.Content.Equals("Sync"))
            {
                Devices.BorderBrush = System.Windows.Media.Brushes.Red;
                Activities.BorderBrush = System.Windows.Media.Brushes.Red;
                Devices.BorderThickness = new Thickness(2, 2, 2, 2);
                Activities.BorderThickness = new Thickness(2, 2, 2, 2);
                MessageBox.Show("No device and/or activity selected!");
            }
            else
            {
                Activities.BorderBrush = System.Windows.Media.Brushes.Gray;
                Activities.BorderBrush = System.Windows.Media.Brushes.Gray;
                Activities.BorderThickness = new Thickness(1, 1, 1, 1);
                Activities.BorderThickness = new Thickness(1, 1, 1, 1);
                Activities_Load();
                Devices_Load();
                if (Sync.Content.Equals("Sync from"))
                {
                    string result = my_man.Sync_From(serial_device, my_user.Email);
                    if (result.Equals(""))
                    {
                        MessageBox.Show("Error syncing activities from the selected device!");
                    }
                    else
                    {
                        MessageBox.Show(result);
                    }
                }
                else
                {
                    bool result = my_man.Sync_To(serial_device, activity_reference, my_user.Email);
                    if (result)
                    {
                        MessageBox.Show("Activity " + activity_reference + " synced successfully !");
                    }
                    else
                    {
                        MessageBox.Show("Error syncing activity " + activity_reference);
                    }
                }
            }
        }

        private void Devices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Sync.Content = "Sync from";
            string model_device = Devices.SelectedItem as string;
            foreach (Device d in lista_devices)
            {
                if (d.Model.Equals(model_device)) serial_device = d.SerialNumber;
            }
        }

        private void Activities_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Devices.SelectedIndex > -1)
            {
                Sync.Content = "Sync to";
                activity_reference = Activities.SelectedItem as string;
            }
            else
            {
                Activities.SelectedIndex = -1;
                Devices.BorderBrush = System.Windows.Media.Brushes.Red;
                Devices.BorderThickness = new Thickness(2, 2, 2, 2);
                MessageBox.Show("No device selected!");
            }
        }
    }
}
