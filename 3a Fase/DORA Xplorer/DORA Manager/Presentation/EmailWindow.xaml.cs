﻿using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Media;

namespace DORA_Manager.Presentation
{
    /// <summary>
    /// Interaction logic for EmailWindow.xaml
    /// </summary>
    public partial class EmailWindow : MetroWindow
    {
        string reference;
        string user;
        Business.DORA_Manager man;

        public EmailWindow(string u, Business.DORA_Manager m, string activity)
        {
            InitializeComponent();
            reference = activity;
            man = m;
            user = u;
        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {
            man.SendEmail(user, Email.Text, reference, Body.Text);
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Email_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Email.Text.Equals("Email"))
            {
                Email.Text = "";
                Email.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Email_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Email.Text.Equals(""))
            {
                Email.Text = "Email";
                Email.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }
    }
}
