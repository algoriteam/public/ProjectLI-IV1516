﻿using Business;
using Data;
using Data.DAO;
using Microsoft.Maps.MapControl.WPF;
using Presentation;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DORA_Manager.Presentation
{
    /// <summary>
    /// Interaction logic for ProfilePage.xaml
    /// </summary>
    public partial class ProfilePage : Page
    {
        Business.DORA_Manager my_man;

        private Photo avatar;
        string street;
        string local;
        string name;
        string username;
        string telephone;
        string facebook;
        ImageSource photo;

        private MapPage my_map;
        private Location my_house;
        private User my_user;
        private MenuWindow my_window;

        public ProfilePage(Business.DORA_Manager man, User user, MenuWindow w)
        {
            InitializeComponent();
            my_man = man;
            my_user = user;
            my_window = w;

            // Obter coordenadas da casa do utilizador
            my_map = new MapPage();
            my_map.Resize(575, 320);
            
            my_house = new Location(user.Latitude,user.Longitude);

            List<Pushpin> aux = my_map.Push_House(my_house,"[Address]\n >Street: "+user.Street+ "\n >Locality: " + user.Locality, user.Email, false);

            my_map.Center_Map(my_house, 17.0);

            InsideFrame.Navigate(my_map);

            // AVATAR
            if (user.Image != null)
            {
                Photo.Source = user.Image;
                photo = user.Image;
            }
            else
                photo = null;

            // NAME
            Name.Text = user.Name;
            name = user.Name;

            // USERNAME
            Username.Text = user.Email;
            username = user.Email;

            // TELEPHONE
            Telephone.Text = user.Phone;
            telephone = user.Phone;

            // FACEBOOK
            Facebook.Text = user.Facebook;
            facebook = user.Facebook;

            // STREET
            Street.Text = user.Street;
            street = user.Street;

            // LOCAL
            Local.Text = user.Locality;
            local = user.Locality;

            // BIRTH
            Birth.SelectedDate = user.Birthday;
            Birth.DisplayMode = CalendarMode.Year;

            // RANKING
            Ranking.Content = Ranking_Solver.FromEnum(my_user.Ranking);

            // STATISTICS
            General_Number_Of_Locations.Content = man.General_Number_Of_Points(my_user.Email).ToString();
            General_Number_Of_Sessions.Content = man.General_Number_Of_Sessions(my_user.Email).ToString();
            General_Total_Distance.Content = man.General_Distance(my_user.Email).ToString();

            // SCOREBOARD
            List<User> scoreboard = man.Scoreboard();
            List<Score_Board_Row> list = new List<Score_Board_Row>();

            int user_pos = -1, pos = 1;
            foreach (User u in scoreboard)
            {
                list.Add(new Score_Board_Row { pos=pos.ToString(), user=u.Email, points=u.Experience.ToString() });
                if (u.Email.Equals(my_user.Email))
                    user_pos = pos;
                pos++;
            }
            Score_Board.ItemsSource = list;
            Position.Content = user_pos.ToString();
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            avatar = my_man.Load_Image();

            if (avatar != null)
            {
                Photo.Source = new BitmapImage(new Uri(avatar.Name));
                photo = new BitmapImage(new Uri(avatar.Name));
            }
        }

        private void Search_Button_Click(object sender, RoutedEventArgs e)
        {
            if (!Street.Text.Equals("") && !Local.Text.Equals(""))
            {
                my_map = new MapPage();
                my_map.Resize(575, 320);
                InsideFrame.Navigate(my_map);

                my_map.first_location = false;
                GeoCoordinate aux = my_map.Geocode(street + " " + local);

                my_house = new Location(aux.Latitude, aux.Longitude);
                my_map.Push_House(my_house, "[Address]\n >Street: " + my_user.Street + "\n >Locality: " + my_user.Locality, my_user.Email, false);
                my_map.Center_Map(my_house, 17.0);
            }
            else
            {
                my_map = new MapPage();
                my_map.Resize(478, 243);
                InsideFrame.Navigate(my_map);

                my_map.first_location = false;
                GeoCoordinate aux = my_map.Geocode(my_user.Street + " " + my_user.Locality);

                my_house = new Location(aux.Latitude, aux.Longitude);
                my_map.Push_House(my_house, "[Address]\n >Street: " + my_user.Street + "\n >Locality: " + my_user.Locality, my_user.Email, false);
                my_map.Center_Map(my_house, 17.0);
            }
        }

        private void Reset_Button_Click(object sender, RoutedEventArgs e)
        {
            my_house = new Location(my_user.Latitude, my_user.Longitude);
            my_map.Push_House(my_house, "[Address]\n >Street: " + my_user.Street + "\n >Locality: " + my_user.Locality, my_user.Email, false);
            my_map.Center_Map(my_house, 17.0);
        }

        private void Username_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Username.Text.Equals(my_user.Email))
            {
                Username.Text = "";
                Username.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Username_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Username.Text.Equals(""))
            {
                Username.Text = my_user.Email;
                Username.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
            else
                username = Username.Text;
        }

        private void Name_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Name.Text.Equals(my_user.Name))
            {
                Name.Text = "";
                Name.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Name_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Name.Text.Equals(""))
            {
                Name.Text = my_user.Name;
                Name.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
            else
                name = Name.Text;
        }

        private void Telephone_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Telephone.Text.Equals(my_user.Phone))
            {
                Telephone.Text = "";
                Telephone.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Telephone_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Telephone.Text.Equals(""))
            {
                Telephone.Text = my_user.Phone;
                Telephone.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
            else
                telephone = Telephone.Text;
        }

        private void Facebook_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Facebook.Text.Equals(my_user.Facebook))
            {
                Facebook.Text = "";
                Facebook.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Facebook_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Facebook.Text.Equals(""))
            {
                Facebook.Text = my_user.Facebook;
                Facebook.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
            else
                facebook = Facebook.Text;
        }

        private void Street_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Street.Text.Equals(my_user.Street))
            {
                Street.Text = "";
                Street.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Street_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Street.Text.Equals(""))
            {
                Street.Text = my_user.Street;
                Street.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
            else street = Street.Text;
        }

        private void Local_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Local.Text.Equals(my_user.Locality))
            {
                Local.Text = "";
                Local.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Local_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Local.Text.Equals(""))
            {
                Local.Text = my_user.Locality;
                Local.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
            else local = Local.Text;
        }

        private void Year_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Year.Text.Equals("Year"))
            {
                Year.Text = "";
                Year.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Year_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Year.Text.Equals(""))
            {
                Year.Text = "Year";
                Year.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        private void Month_GotFocus(object sender, RoutedEventArgs e)
        {
            if (Month.Text.Equals("Month"))
            {
                Month.Text = "";
                Month.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        private void Month_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Month.Text.Equals(""))
            {
                Month.Text = "Month";
                Month.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(156, 155, 155, 1));
            }
        }

        public byte[] BitmapToByteArray(Bitmap image)
        {
            if (image != null)
            {
                byte[] data;

                using (var memoryStream = new MemoryStream())
                {
                    image.Save(memoryStream, ImageFormat.Bmp);

                    data = memoryStream.ToArray();
                }
                return data;
            }
            else
                return null;
        }

        private void Ok_Button_Click_1(object sender, RoutedEventArgs e)
        {
            GeoCoordinate aux = my_map.Geocode(street + " " + local);
            my_house = new Location(aux.Latitude, aux.Longitude);
            my_map.Push_House(my_house, "[Address]\n >Street: " + my_user.Street + "\n >Locality: " + my_user.Locality, my_user.Email, false);
            my_map.Center_Map(my_house, 17.0);

            User aux_user = new User(name, username, facebook, telephone, my_user.Password, Birth.SelectedDate.Value, photo, my_user.Ranking, my_user.Experience, street, local, true, my_house.Latitude, my_house.Longitude);

            my_window.SetUser(aux_user);

            UserDAO users = new UserDAO();
            users[my_user.Email] = aux_user;
            MessageBox.Show("Little DORA made your changes successfully");
        }

        private void Ok_Statistics_Button_Click(object sender, RoutedEventArgs e)
        {
            if ((Year.Text.Equals("") || Year.Text.Equals("Year")) && (Month.Text.Equals("") || Month.Text.Equals("Month")))
                MessageBox.Show("Dados Introduzidos Incorretos");
            else if (Month.Text.Equals("") || Month.Text.Equals("Month"))
            {
                Year_Month_Number_Of_Locations.Content = my_man.Anual_Number_Of_Points(my_user.Email, int.Parse(Year.Text));
                Year_Month_Number_Of_Sessions.Content = my_man.Anual_Number_Of_Sessions(my_user.Email, int.Parse(Year.Text));
                Year_Month_Distance.Content = my_man.Anual_Distance(my_user.Email, int.Parse(Year.Text));
            }
            else
            {
                Year_Month_Number_Of_Locations.Content = my_man.Monthly_Number_Of_Points(my_user.Email, int.Parse(Year.Text), int.Parse(Month.Text));
                Year_Month_Number_Of_Sessions.Content = my_man.Monthly_Number_Of_Sessions(my_user.Email, int.Parse(Year.Text), int.Parse(Month.Text));
                Year_Month_Distance.Content = my_man.Monthly_Distance(my_user.Email, int.Parse(Year.Text), int.Parse(Month.Text));
            }
        }
    }

    public class Score_Board_Row
    {
        public string pos { get; set; }
        public string user { get; set; }
        public string points { get; set; }
    }
}
