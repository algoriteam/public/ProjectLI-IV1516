﻿using Data;
using Data.DAO;
using DORA_Manager.Presentation;
using Managed.Adb;
using PdfSharp.Pdf;
using Shell32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;

namespace Business
{
    public class DORA_Manager
    {
        UserDAO users = new UserDAO();
        PointDAO points = new PointDAO();
        PhotoDAO photos = new PhotoDAO();
        ActivityDAO activities = new ActivityDAO();
        UserPointDAO user_points = new UserPointDAO();
        ActivityPointDAO activity_points = new ActivityPointDAO();

        // Auxiliares

        public User getUser(string mail)
        {
            User aux;
            users.TryGetValue(mail, out aux);
            return aux;
        }

        public List<Activity> Get_Activities_Of_User(string email)
        {
            List<Activity> aux = new List<Activity>();
            
            foreach (Activity a in activities.Values)
            {
                if (a.Email.Equals(email))
                {
                    aux.Add(Get_Activity_Single(a.Reference));
                }
            }
            return aux;
        }

        public List<Point> Get_Points_Of_User(string user)
        {
            List<Point> aux = new List<Point>();
            List<Activity> acts = Get_Activities_Of_User(user);
            List<string> refs = new List<string>();

            foreach (Activity a in acts)
                refs.Add(a.Reference);

            foreach (UserPoint u in user_points.Values)
            {
                if (u.Email.Equals(user))
                    aux.Add(u.Coordinates);
            }

            foreach (ActivityPoint p in activity_points)
            {
                if (aux.Contains(p.Coordinates) && refs.Contains(p.Reference) && !Is_Finished(Get_Activity(p.Reference)))
                {
                    aux.Remove(p.Coordinates);
                }
            }

            return aux;
        }

        public List<Point> Get_Points()
        {
            List<Point> aux = new List<Point>();

            var enumerator = points.GetEnumerator();

            while (enumerator.MoveNext())
                aux.Add(enumerator.Current);

            return aux;
        }

        public string Get_User_Point_Name(string user, Point p)
        {
            UserCoordinates c = new UserCoordinates(user, p);
            var enumerator = user_points.GetEnumerator();

            while (enumerator.MoveNext())
                if (enumerator.Current.Key.Equals(c))
                    return enumerator.Current.Value.Name;
            return "";
        }

        public string Get_User_Point_Description(string user, Point p)
        {
            UserCoordinates c = new UserCoordinates(user, p);
            var enumerator = user_points.GetEnumerator();

            while (enumerator.MoveNext())
                if (enumerator.Current.Key.Equals(c))
                    return enumerator.Current.Value.Description;
            return "";
        }

        // Requisito 1

        public int Login(string email, string password)
        {
            // 0 -> Correu tudo bem! 
            // 1 -> Password ou Mail incorretos
            // 2 -> Ja esta autenticado

            try
            {
                User aux_user = users[email];

                if (aux_user != null && aux_user.Password.Equals(Encrypt(password)))
                {
                    if (aux_user.Logged_in)
                        return 2;
                    else
                    {
                        aux_user.Logged_in = true;
                        users[email] = aux_user;
                        return 0;
                    }
                }
                else
                    return 1;
            }
            catch (KeyNotFoundException e)
            {
                return 1;
            }
            catch (ArgumentNullException e)
            {
                return 1;
            }
        }

        public void Logout(string email)
        {
            User aux = users[email];
            aux.Logged_in = false;
            users[email] = aux;
        }

        public string Encrypt(string password)
        {
            char[] aux = password.ToCharArray();

            for (int i = 0; i < aux.Length; i++)
                aux[i] += 'a';

            return new string(aux);
        }

        public string Decrypt(string password)
        {
            char[] aux = password.ToCharArray();

            for (int i = 0; i < aux.Length; i++)
                aux[i] -= 'a';

            return new string(aux);
        }

        // Requisito 2

        public bool Register(string email, string password, string name, string street, string locality, double latitude, 
            double longitude, DateTime birthday, ImageSource image, string facebook, string phone)
        {
            User aux;

            try
            {
                aux = users[email];
                return false;
            }
            catch (KeyNotFoundException e)
            {
                aux = new User(name, email, facebook ,phone, Encrypt(password), birthday, image, Ranking.Beginner, 0, street, 
                    locality, false, latitude, longitude);
                users.Add(email, aux);
                return true;
            }
        }

        public Photo Load_Image()
        {
            Photo photo;

            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            
            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".jpeg";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*.jpeg";
            
            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();
            
            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                BitmapImage image = new BitmapImage(new Uri(filename));
                photo = new Photo(filename, image);
                return photo;
            }
            else
                return null;
        }

        // Requisito 3

        public Activity Create_Activity(string email, List<Point> ps, double distance)
        {
            string rf = GenerateReference();
            List<UserPoint> pts = new List<UserPoint>();
            Activity this_activity = new Activity(rf, 0, new DateTime(), null, email, pts, distance);
            activities.Add(rf, this_activity);

            int i = 1;
            foreach (Point l in ps)
            {
                Point p = new Point(l.Latitude, l.Longitude);
                points.Add(p);

                // Adicionar à tabela de Pontos de Sessão
                ActivityPoint act_pt = new ActivityPoint(p, rf, i++);
                activity_points.Add(act_pt);

                // Adicionar à lista de Pontos da Sessão
                UserPoint user_pt;
                user_points.TryGetValue(new UserCoordinates(email, p), out user_pt);

                if (user_pt == null)
                {
                    user_pt = new UserPoint(p, email, "", "", null);
                }

                List<Photo> ph = user_pt.Photos;
                photos.TryGetValue(user_pt, out ph);
                pts.Add(user_pt);

                try
                {
                    user_points.Add(new UserCoordinates(email, user_pt.Coordinates), user_pt);
                }
                catch (ArgumentException e)
                {
                    user_points[new UserCoordinates(email, user_pt.Coordinates)] = user_pt;
                }
            }
            this_activity.Points = pts;
            this_activity.Remake_Report();
            activities[rf] = this_activity;

            return this_activity;
        }

        public Activity Create_Activity(string rf, string email, List<Point> ps, double distance)
        {
            List<UserPoint> pts = new List<UserPoint>();
            Activity this_activity = new Activity(rf, 0, new DateTime(), null, email, pts, distance);
            activities.Add(rf, this_activity);

            foreach (Point l in ps)
            {
                Point p = new Point(l.Latitude, l.Longitude);
                points.Add(p);

                // Adicionar à tabela de Pontos de Sessão
                activity_points.Add((ActivityPoint) l);

                // Adicionar à lista de Pontos da Sessão

                UserPoint user_pt;
                user_points.TryGetValue(new UserCoordinates(email, p), out user_pt);

                if (user_pt == null)
                {
                    user_pt = new UserPoint(p, email, "", "", null);
                }

                List<Photo> ph = user_pt.Photos;
                photos.TryGetValue(user_pt, out ph);
                pts.Add(user_pt);

                try
                {
                    user_points.Add(new UserCoordinates(email, user_pt.Coordinates), user_pt);
                }
                catch (Exception e)
                {
                    user_points[new UserCoordinates(email, user_pt.Coordinates)] = user_pt;
                }
            }
            this_activity.Points = pts;
            activities[rf] = this_activity;

            return this_activity;
        }

        public int Calculate_Pontuation(List<UserPoint> pts, string email)
        {
            int pt = 0;

            foreach (UserPoint p in pts)
            {
                int aux = 0;

                foreach (ActivityPoint act_pt in activity_points)
                    if (act_pt.Coordinates.Equals(p.Coordinates) && Is_Finished(Get_Activity(act_pt.Reference)))
                        aux++;

                if (aux == 0)
                {
                    p.First = true;
                    pt += 8;
                }
                else
                {
                    p.First = false;
                    pt += 2;
                }

                if (p.Name != null && !p.Name.Equals(""))
                    pt += 2;
                if (p.Description != null && !p.Description.Equals(""))
                    pt += 2;
                if (p.Photos != null)
                    pt += p.Photos.Count;
            }
            return pt;
        }

        // Requisito 4

        public string GenerateReference()
        {
            string possibilities = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            char[] chars = possibilities.ToCharArray();
            Random r = new Random();
            StringBuilder reference = new StringBuilder();

            try
            {
                while (true)
                {
                    reference = new StringBuilder();
                    reference.Append("#DORA");

                    for (int i = 5; i < 9; i++) {
                        reference.Append(chars[r.Next(0, 62)]);
                    }

                    Activity aux = activities[reference.ToString()];
                }
            }
            catch (KeyNotFoundException e)
            {
                return reference.ToString();
            }
        }

        // Requisito 5

        public PdfDocument Get_Pdf_Of_Section(Activity act)
        {
            return act.Pretty_Printing();
        }

        // Requisito 6

        public bool Is_Finished(Activity act)
        {
            bool aux;
            DateTime date = act.Date;

            if (date.Year == 0001 && date.Month == 01 && date.Day == 01)
                aux = false;
            else
                aux = true;
            return aux;
        }

        // Requisito 7

        public bool Sync_To(string serialNumber, string rf, string user)
        {
            Activity aux = Get_Activity_Photos(rf, user);
            aux.SortPoints(new UserPointComparator(user));
            aux.Remake_Report();
            File.WriteAllText(Environment.CurrentDirectory + "\\ManagerReport.xml", aux.Report);

            string strCmdText = "adb -s " + serialNumber + " push ManagerReport.xml /sdcard/DORA/" + rf + "/ManagerReport.xml";
            System.Diagnostics.Process.Start("CMD.exe", "/C " + strCmdText);
            return true;
        }

        // Requisito 8

        public Activity Get_Activity(string rf)
        {
            Activity act = activities[rf];
            List<UserPoint> pts = new List<UserPoint>();
            List<ActivityPoint> aux = new List<ActivityPoint>();

            foreach (ActivityPoint a in activity_points)
            {
                if (a.Reference.Equals(rf))
                    aux.Add(a);
            }
            aux.Sort(new ActivityPointComparator());

            foreach (ActivityPoint a in aux)
            {
                if (a.Reference.Equals(rf))
                {
                    UserPoint p;
                    user_points.TryGetValue(new UserCoordinates(act.Email, a.Coordinates), out p);
                    pts.Add(p);
                    p.Photos = new List<Photo>();
                    List<Photo> ph;
                    photos.TryGetValue(p, out ph);
                    p.Photos = ph;
                }
            }
            act.Points = pts;
            return act;
        }

        public Activity Get_Activity_Photos(string rf, string user)
        {
            Activity act = activities[rf];
            List<UserPoint> pts = new List<UserPoint>();
            List<ActivityPoint> aux = new List<ActivityPoint>();

            foreach (ActivityPoint a in activity_points)
            {
                if (a.Reference.Equals(rf))
                    aux.Add(a);
            }

            foreach (ActivityPoint a in aux)
            {
                if (a.Reference.Equals(rf))
                {
                    UserPoint p;
                    user_points.TryGetValue(new UserCoordinates(act.Email, a.Coordinates), out p);
                    p.Email = user;
                    pts.Add(p);
                    p.Photos = new List<Photo>();
                    List<Photo> ph;
                    photos.TryGetValue(p, out ph);
                    p.Photos = ph;
                }
            }
            act.Points = pts;
            act.Remake_Report();
            return act;
        }
        
        public Activity Get_Activity_Single(string rf)
        {
            Activity act = activities[rf];
            List<UserPoint> pts = new List<UserPoint>();
            List<ActivityPoint> aux = new List<ActivityPoint>();

            foreach (ActivityPoint a in activity_points)
            {
                if (a.Reference.Equals(rf))
                    aux.Add(a);
            }

            foreach (ActivityPoint a in aux)
            {
                if (a.Reference.Equals(rf))
                {
                    UserPoint p;
                    UserCoordinates uc = new UserCoordinates(act.Email, new Point(a.Latitude, a.Longitude));
                    p = user_points[uc];
                    pts.Add(p);
                }
            }

            act.Points = pts;
            return act;
        }

        // Requisito 9

        public List<Device> GetDevices()
        {
            AndroidDebugBridge mADB = AndroidDebugBridge.CreateBridge("C:\\Android\\sdk\\platform-tools\\adb.exe", true);
            mADB.Start();

            return AdbHelper.Instance.GetDevices(AndroidDebugBridge.SocketAddress);
        }

        public string Sync_From(string serialNumber, string user)
        {
            string strCmdText = "adb -s " + serialNumber + " pull /sdcard/DORA";
            System.Diagnostics.Process.Start("CMD.exe", "/C " + strCmdText);
            Thread.Sleep(1000);
            StringBuilder results = new StringBuilder();
            
            if (Directory.Exists("DORA"))
            {
                foreach (string d in Directory.GetDirectories(Environment.CurrentDirectory + "\\DORA"))
                {
                    if (File.Exists(d + "\\TrackerReport.xml"))
                    {
                        // Após ter ficheiros sincronizados, parse do xml (para cada ficheiro)
                        Activity act;
                        string aux_value;
                        string xml = File.ReadAllText(d + "\\TrackerReport.xml");

                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(xml);

                        XmlNode node = doc.DocumentElement.SelectSingleNode("/Activity");
                        string reference = node.Attributes["ref"]?.InnerText;
                        int nr_points = int.Parse(node.Attributes["points"]?.InnerText);
                        DateTime date = DateTime.Parse(node.Attributes["date"]?.InnerText);
                        aux_value = node.Attributes["distance"]?.InnerText;
                        aux_value = aux_value.Trim(new char[] { '\"' });
                        aux_value = aux_value.Replace('.', ',');
                        double distance = Convert.ToDouble(aux_value);
                        bool aux_voice = bool.Parse(node.Attributes["voice"]?.InnerText);

                        List<UserPoint> pts = new List<UserPoint>();

                        for (int i = 1; i <= nr_points; i++)
                        {
                            XmlNode point = doc.DocumentElement.SelectSingleNode("/Activity/Point[" + i + "]");
                            aux_value = point.Attributes["latitude"]?.InnerText;
                            aux_value = aux_value.Trim(new char[] { '\"' });
                            aux_value = aux_value.Replace('.', ',');
                            double latitude = Convert.ToDouble(aux_value);

                            aux_value = point.Attributes["longitude"]?.InnerText;
                            aux_value = aux_value.Trim(new char[] { '\"' });
                            aux_value = aux_value.Replace('.', ',');
                            double longitude = Convert.ToDouble(aux_value);

                            int nr_photos = int.Parse(point.Attributes["photos"]?.InnerText);

                            XmlNode nameNode = doc.DocumentElement.SelectSingleNode("/Activity/Point[" + i + "]/Name");
                            string name = nameNode.InnerText;

                            XmlNode descriptionNode = doc.DocumentElement.SelectSingleNode("/Activity/Point[" + i + "]/Description");
                            string description = descriptionNode.InnerText;

                            List<Photo> phts = new List<Photo>();
                            foreach (XmlNode photo in doc.DocumentElement.SelectNodes("/Activity/Point[" + i + "]/Photos/Photo"))
                            {
                                string phname = photo.Attributes["name"]?.InnerText;
                                Photo ph = new Photo(phname, File.ReadAllBytes(d + "\\" + phname));
                                phts.Add(ph);
                            }

                            UserPoint u = new UserPoint(new Point(latitude, longitude), user, name, description, phts);
                            photos.Add(u, phts);

                            user_points[new UserCoordinates(user, new Point(latitude, longitude))] = u;
                            pts.Add(u);
                        }

                        byte[] voice = null;

                        if (aux_voice)
                            voice = File.ReadAllBytes(d + "\\voice.wav");

                        act = new Activity(reference, 0, date, voice, user, pts, distance);
                        act.Experience = Calculate_Pontuation(pts, user);
                        act.Voice_register = voice;
                        act.Remake_Report();
                        activities[reference] = act;

                        results.Append("Activity " + reference + " ->   + " + act.Experience + " EXP\n");

                        strCmdText = "adb -s " + serialNumber + " shell rm -rf /sdcard/DORA/" + reference + "/";
                        System.Diagnostics.Process.Start("CMD.exe", "/C " + strCmdText);
                    }
                }
                DirectoryInfo di = new DirectoryInfo("DORA");

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                di.Delete();
            }
            return results.ToString();
        }

        // Requisito 10

        public bool Remove_Activity(string rf, string user)
        {
            bool aux;
            Activity act;
            activities.TryGetValue(rf, out act);

            if (Is_Finished(act))
                aux = false;
            else
            {
                // Pontos de Atividade
                var enum_act = activity_points.GetEnumerator();

                while (enum_act.MoveNext())
                    if (enum_act.Current.Reference.Equals(rf))
                        activity_points.Remove(enum_act.Current);

                // Pontos de Utilizador
                var enum_pts = user_points.GetEnumerator();

                while (enum_pts.MoveNext())
                {
                    if (enum_pts.Current.Value.Email.Equals(user) && enum_pts.Current.Value.Only_Reference(rf))
                    {
                        photos.Remove(enum_pts.Current.Value);
                        user_points.Remove(enum_pts.Current);
                    }
                }
                activities.Remove(rf);
                aux = true;
            }
            return aux;
        }

        // Requisito 11

        public void SendEmail(string user, string receiver, string rf, string body)
        {
            Attachment attachment;
            Activity act = Get_Activity_Photos(rf, user);
            MailMessage mail = new MailMessage();

            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("doraxplorer.services@gmail.com");
            mail.To.Add(receiver);
            mail.Subject = "DORA Report - Activity " + rf;
            mail.Body = body + "\n\nSent by user " + user;
            
            PdfDocument pdf = Get_Pdf_Of_Section(act);
            pdf.Save(rf + ".pdf");
            attachment = new Attachment(rf + ".pdf");
            mail.Attachments.Add(attachment);

            foreach (UserPoint u in act.Points)
            {
                try
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        File.WriteAllBytes("(" + u.Latitude + ";" + u.Longitude + ")" + i + ".jpeg", ImageSourceToBytes(new JpegBitmapEncoder(), u.Photos[i-1].Image));
                        attachment = new Attachment("(" + u.Latitude + ";" + u.Longitude + ")" + i + ".jpeg");
                        mail.Attachments.Add(attachment);
                    }
                }
                catch (ArgumentOutOfRangeException) { }
            }

            if (act.Voice_register != null)
            {
                File.WriteAllBytes(rf + ".wav", act.Voice_register);
                attachment = new Attachment(rf + ".wav");
                mail.Attachments.Add(attachment);
            }

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("doraxplorer.services@gmail.com", "apenastexugosaqui");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            System.Windows.MessageBox.Show("Email delivered with sucess!");
        }

        public byte[] ImageSourceToBytes(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }
            return bytes;
        }

        // Requisito 12

        public bool Update_Name(string email, string name)
        {
            if (name == null)
                return false;
            else
            {
                User aux = users[email];
                aux.Name = name;
                users[email] = aux;
                return true;
            }
        }

        public bool Update_Address(string email, string street, string locality, double latitude, double longitude)
        {
            if (street == null)
                return false;
            else
            {
                User aux = users[email];
                aux.Street = street;
                aux.Locality = locality;
                aux.Latitude = latitude;
                aux.Longitude = longitude;
                users[email] = aux;
                return true;
            }
        }

        // Requisito 13

        public int General_Number_Of_Points(string email)
        {
            int points = 0;

            List<ActivityPoint> aux = new List<ActivityPoint>();
            List<string> acts = new List<string>();
            foreach (Activity act in activities.Values)
                if (act.Email.Equals(email))
                    acts.Add(act.Reference);

            foreach (ActivityPoint act in activity_points)
            {
                if (acts.Contains(act.Reference) && Is_Finished(Get_Activity(act.Reference)))
                    aux.Add(act);
            }
            points = aux.Count;
            return points;
        }

        public int Anual_Number_Of_Points(string email, int year)
        {
            int points = 0;
            List<UserPoint> aux = user_points.Points_From_User(email);

            foreach (UserPoint u in aux)
            {
                foreach (ActivityPoint a in activity_points)
                {
                    if (u.Coordinates.Equals(a.Coordinates))
                    {
                        string rf = a.Reference;
                        Activity act = activities[rf];
                        DateTime date = act.Date;

                        if (date.Year == year && Is_Finished(act))
                            points++;
                    }
                }
            }
            return points;
        }

        public int Monthly_Number_Of_Points(string email, int year, int month)
        {
            int points = 0;
            List<UserPoint> aux = user_points.Points_From_User(email);

            foreach (UserPoint u in aux)
            {
                foreach (ActivityPoint a in activity_points)
                {
                    if (u.Coordinates.Equals(a.Coordinates))
                    {
                        string rf = a.Reference;
                        Activity act = activities[rf];
                        DateTime date = act.Date;

                        if (date.Year == year && date.Month == month && Is_Finished(act))
                            points++;
                    }
                }
            }
            return points;
        }

        public int General_Number_Of_Sessions(string email)
        {
            int points = 0;

            foreach (Activity act in activities.Values)
                if (act.Email.Equals(email) && Is_Finished(act))
                    points++;
            return points;
        }

        public int Anual_Number_Of_Sessions(string email, int year)
        {
            int points = 0;

            foreach (Activity act in activities.Values)
                if (act.Email.Equals(email) && act.Date.Year == year && Is_Finished(act))
                    points++;
            return points;
        }

        public int Monthly_Number_Of_Sessions(string email, int year, int month)
        {
            int points = 0;

            foreach (Activity act in activities.Values)
                if (act.Email.Equals(email) && act.Date.Year == year && act.Date.Month == month && Is_Finished(act))
                    points++;
            return points;
        }

        public double General_Distance(string email)
        {
            double distance = 0.0;

            foreach (Activity act in activities.Values)
                if (act.Email.Equals(email) && Is_Finished(act))
                    distance += act.Distance; ;
            return distance;
        }

        public double Anual_Distance(string email, int year)
        {
            double distance = 0.0;

            foreach (Activity act in activities.Values)
                if (act.Email.Equals(email) && act.Date.Year == year && Is_Finished(act))
                    distance += act.Distance; ;
            return distance;
        }

        public double Monthly_Distance(string email, int year, int month)
        {
            double distance = 0.0;

            foreach (Activity act in activities.Values)
                if (act.Email.Equals(email) && act.Date.Year == year && act.Date.Month == month && Is_Finished(act))
                    distance += act.Distance; ;
            return distance;
        }

        // Requisito 14

        public List<User> Scoreboard()
        {
            List<User> aux = new List<User>();
            aux = (List<User>) users.Values;
            aux.Sort(new ScoreComparator());
            return aux;
        }

        // Requisito 15

        public int Find_Point(string email, UserPoint p, int photos)
        {
            // 0 -> nunca foi encontrado
            // 1 -> já foi encontrado por alguem mas nao pelo utilizador
            // 2 -> ja foi encontrado pelo utilizador
            // -1 -> asneira

            int acc_points = 0, res = -1;

            // points.Add(p.Coordinates);
            try
            {
                var pt = user_points[new UserCoordinates(email, p.Coordinates)];
                res = 2;

                string old_name = pt.Name;
                string old_description = pt.Description;

                if (old_name == null && p.Name != null)
                    acc_points += 2;
                if (old_description == null && p.Description != null)
                    acc_points += 2;
                // Contar fotos
                // ...
            }
            catch (KeyNotFoundException e)
            {
                acc_points += 2;

                // Um ponto só existe a BD se já tiver sido descoberto!
               /* if (!points.Contains(p.Coordinates))
                {
                    points.Add(p.Coordinates);
                    acc_points += 6;
                    res = 0;
                }
                else
                    res = 1;*/
            }
            return res;
        }
    }

    public class ScoreComparator : IComparer<User>
    {
        public int Compare(User x, User y)
        {
            if (x.Experience < y.Experience)
                return 1;
            else if (x.Experience == y.Experience)
                return 0;
            else
                return -1;
        }
    }

    public class ActivityPointComparator : IComparer<ActivityPoint>
    {
        public int Compare(ActivityPoint x, ActivityPoint y)
        {
            if (x.Order < y.Order)
                return 1;
            else if (x.Order == y.Order)
                return 0;
            else
                return -1;
        }
    }
}
