﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class UserCoordinates : Point
    {
        private string email;

        public UserCoordinates(string e, Point c) : base(c.Latitude, c.Longitude)
        {
            Email = e;
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public Point Coordinates
        {
            get
            {
                return new Point(Latitude, Longitude);
            }

            set
            {
                Latitude = value.Latitude;
                Longitude = value.Longitude;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;
            if ((obj == null) || (!this.GetType().Name.Equals(obj.GetType().Name)))
                return false;
            else
            {
                UserCoordinates aux = (UserCoordinates) obj;

                return base.Equals(aux) &&
                    email.Equals(aux.Email);
            }
        }
    }
}
