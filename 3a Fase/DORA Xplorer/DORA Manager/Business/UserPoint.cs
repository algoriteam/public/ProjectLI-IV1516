using Data.DAO;
using System;
using System.Collections.Generic;

namespace Business
{
    public class UserPoint : Point
    {
        private string email;
        private string name;
        private string description;
        private bool first;
        private List<Photo> photos;

        public UserPoint(Point c, string e, string n, string d, List<Photo> p) : base(c)
        {
            Email = e;
            Name = n;
            Description = d;
            Photos = p;
            First = false;
        }

        public UserPoint(Point c, string e, string n, string d, bool f, List<Photo> p) : base(c)
        {
            Email = e;
            Name = n;
            Description = d;
            First = f;
            Photos = p;
        }

        public Point Coordinates
        {
            get
            {
                return new Point(Latitude, Longitude);
            }

            set
            {
                Latitude = value.Latitude;
                Longitude = value.Longitude;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public bool First
        {
            get
            {
                return first;
            }

            set
            {
                first = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public List<Photo> Photos
        {
            get
            {
                return photos;
            }

            set
            {
                photos = value;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object o)
        {
            if (this == o)
                return true;
            if ((o == null) || (!this.GetType().Name.Equals(o.GetType().Name)))
                return false;
            else
            {
                UserPoint aux = (UserPoint) o;

                return base.Equals(aux) &&
                    Email.Equals(aux.Email) &&
                    name.Equals(aux.Name) &&
                    description.Equals(aux.Description) &&
                    first == aux.First;
            }
        }

        public bool Only_Reference(string reference)
        {
            bool aux = true;
            var act_pts = new ActivityPointDAO();

            var enumerator = act_pts.GetEnumerator();

            while (enumerator.MoveNext() && aux)
                if (enumerator.Current.Coordinates.Equals(Coordinates) && !enumerator.Current.Reference.Equals(reference))
                    aux = false;

            return aux;
        }
    }
}
