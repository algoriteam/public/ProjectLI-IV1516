using Data.DAO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business
{
    public class Point
    {
        private double latitude;
        private double longitude;

        public Point()
        {
            latitude = longitude = 0.0;
        }

        public Point(double latitude, double longitude)
        {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public Point(Point c)
        {
            this.latitude = c.Latitude;
            this.longitude = c.Longitude;
        }

        public Point Clone()
        {
            return new Point(this);
        }

        public double Latitude
        {
            get
            {
                return latitude;
            }

            set
            {
                latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return longitude;
            }

            set
            {
                longitude = value;
            }
        }

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.Latitude + b.Latitude, a.Longitude + b.Longitude);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object o)
        {
            if (this == o)
                return true;
            if ((o == null) || (!this.GetType().Name.Equals(o.GetType().Name)))
                return false;
            else
            {
                Point aux = (Point)o;

                return latitude == aux.Latitude && longitude == aux.Longitude;
            }
        }

        public bool Discovered_By_User(DORA_Manager man, List<string> refs, List<ActivityPoint> points, string user)
        {
            bool aux = false;

            foreach (ActivityPoint p in points)
            {
                if (p.Coordinates.Equals(this) && refs.Contains(p.Reference) && man.Is_Finished(man.Get_Activity(p.Reference)))
                {
                    aux = true;
                    break;
                }
            }

            return aux;
        }

        public bool User_Is_First(string user)
        {
            bool aux = false;
            UserPointDAO pts = new UserPointDAO();

            foreach (UserPoint p in pts.Values)
            {
                if (p.Coordinates.Equals(this) && p.Email.Equals(user) && p.First)
                {
                    aux = true;
                    break;
                }
            }
            return aux;
        }

        public override string ToString()
        {
            StringBuilder aux = new StringBuilder();

            aux.Append("Latitude: ");
            aux.Append(latitude);
            aux.Append("\nLongitude: ");
            aux.Append(longitude);

            return aux.ToString();
        }

        private static readonly double RADIUS = 6371; // Raio da Terra

        private double Haversine(double teta)
        {
            return ((1 - Math.Cos(teta)) * (Math.Pow(2, -1)));
        }

        public double Distance(Point p)
        {
            double lat1 = p.Latitude;
            double long1 = p.Longitude;

            return 2 * RADIUS * Math.Asin(Math.Sqrt(Haversine(ToRadians(lat1) - ToRadians(latitude))
                                              + Math.Cos(ToRadians(latitude)) * Math.Cos(ToRadians(lat1))
                                              * Haversine(ToRadians(long1) - ToRadians(longitude))));
        }

        public double ToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }

        public bool Discovered(DORA_Manager man, List<ActivityPoint> points)
        {
            bool aux = false;

            foreach (ActivityPoint p in points)
            {
                if (p.Coordinates.Equals(this) && man.Is_Finished(man.Get_Activity(p.Reference)))
                {
                    aux = true;
                    break;
                }
            }

            return aux;
        }
    }
}
