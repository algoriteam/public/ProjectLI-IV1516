using DORA_Manager.Presentation;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Business
{
    [Serializable]
    public class Activity
    {
        private string reference;
        private int experience;
        private DateTime date;
        private byte[] voice_register;
        private string report;
        private string email;
        private List<UserPoint> points;
        private double distance;

        public Activity()
        {
            reference = "";
            experience = 0;
            date = new DateTime();
            voice_register = null;
            report = "";
            email = "";
            Distance = 0.0;
        }

        public Activity(string rf, int p, DateTime d, byte[] vr, string rep, string e, double dt)
        {
            reference = rf;
            experience = p;
            date = d;

            if (vr != null)
            {
                voice_register = new byte[vr.Length];
                for (int i = 0; i < vr.Length; i++)
                    voice_register[i] = vr[i];
            }
            else
                voice_register = null;

            report = rep;
            email = e;
            Distance = dt;
        }

        public Activity(string rf, int p, DateTime d, byte[] vr, string rep, string e, List<UserPoint> pts, double dt)
        {
            reference = rf;
            experience = p;
            date = d;

            if (vr != null)
            {
                voice_register = new byte[vr.Length];
                for (int i = 0; i < vr.Length; i++)
                    voice_register[i] = vr[i];
            }
            else
                voice_register = null;

            report = rep;
            email = e;

            if (pts != null)
            {
                points = new List<UserPoint>();
                foreach (UserPoint pt in pts)
                    points.Add(pt);
                SortPoints(new UserPointComparator(reference));
            }
            else
                points = null;
            Distance = dt;
        }

        public Activity(string rf, int p, DateTime d, byte[] vr, string e, List<UserPoint> pts, double dt)
        {
            reference = rf;
            experience = p;
            date = d;

            if (vr != null)
            {
                voice_register = new byte[vr.Length];
                for (int i = 0; i < vr.Length; i++)
                    voice_register[i] = vr[i];
            }
            else
                voice_register = null;

            email = e;

            if (pts != null)
            {
                points = new List<UserPoint>();
                foreach (UserPoint pt in pts)
                    points.Add(pt);
                SortPoints(new UserPointComparator(reference));
                StringBuilder aux = new StringBuilder();
                aux.Append("<Activity ref=\"");
                aux.Append(reference);
                aux.Append("\" experience=\"");
                aux.Append(experience);
                aux.Append("\" points=\"");
                aux.Append(points.Count);
                aux.Append("\">\n");

                foreach (UserPoint pt in points)
                {
                    aux.Append("\t<Point latitude=\"");
                    aux.Append(pt.Coordinates.Latitude.ToString(CultureInfo.InvariantCulture));
                    aux.Append("\" longitude=\"");
                    aux.Append(pt.Coordinates.Longitude.ToString(CultureInfo.InvariantCulture));
                    aux.Append("\" photos=\"");
                    if (pt.Photos != null)
                        aux.Append(pt.Photos.Count);
                    else
                        aux.Append("0");
                    aux.Append("\">\n");
                    aux.Append("\t\t<Name>\n\t\t\t");
                    aux.Append(pt.Name);
                    aux.Append("\n\t\t</Name>\n\t\t<Description>\n\t\t\t");
                    aux.Append(pt.Description);
                    aux.Append("\n\t\t</Description>\n\t</Point>\n");
                }
                aux.Append("</Activity>");
                report = aux.ToString();
            }
            else
            {
                points = null;
                StringBuilder aux = new StringBuilder();
                aux.Append("<Activity ref=\"");
                aux.Append(reference);
                aux.Append("\" distance=\"");
                aux.Append(distance.ToString(CultureInfo.InvariantCulture));
                aux.Append("\" experience=\"");
                aux.Append(experience);
                aux.Append("\" points=\"");
                aux.Append(points.Count);
                aux.Append("\">\n");
                aux.Append("</Activity>");
                report = aux.ToString();
            }
            Distance = dt;
        }

        public string Reference
        {
            get
            {
                return reference;
            }

            set
            {
                reference = value;
            }
        }

        public int Experience
        {
            get
            {
                return experience;
            }

            set
            {
                experience = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }

        public byte[] Voice_register
        {
            get
            {
                return voice_register;
            }

            set
            {
                voice_register = value;
            }
        }

        public string Report
        {
            get
            {
                return report;
            }

            set
            {
                report = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public List<UserPoint> Points
        {
            get
            {
                return points;
            }

            set
            {
                points = value;
                SortPoints(new UserPointComparator(reference));
            }
        }

        public double Distance
        {
            get
            {
                return distance;
            }

            set
            {
                distance = value;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object o)
        {
            if (this == o)
                return true;
            if ((o == null) || (!this.GetType().Name.Equals(o.GetType().Name)))
                return false;
            else
            {
                Activity aux = (Activity)o;

                bool pts = true;
                foreach (UserPoint pt in points)
                {
                    if (!aux.Points.Contains(pt))
                    {
                        pts = false;
                        break;
                    }
                }

                return reference.Equals(aux.Reference) &&
                    experience == aux.experience &&
                    date.Equals(aux.Date) &&
                    report.Equals(aux.Report) &&
                    email.Equals(aux.Email) &&
                    pts;
            }
        }

        public override string ToString()
        {
            return reference;
        }

        public PdfDocument Pretty_Printing()
        {
            bool first = true;
            PdfDocument pdf = new PdfDocument();
            PdfPage pdfPage = pdf.AddPage();

            XGraphics graph = XGraphics.FromPdfPage(pdfPage);
            XFont fontTitle = new XFont("Arial", 26, XFontStyle.Bold);
            XFont fontSubTitle = new XFont("Arial", 20, XFontStyle.Bold);
            XFont fontText = new XFont("Arial", 16, XFontStyle.Regular);
            XFont fontAlgoriteam = new XFont("Arial", 8, XFontStyle.Bold);
            XTextFormatter tf = new XTextFormatter(graph);

            tf.DrawString("DORA Report", fontTitle, XBrushes.Black,
                new XRect(20, 20, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.TopLeft);

            XImage img;

            tf.DrawString("Activity " + reference, fontSubTitle, XBrushes.Black,
                new XRect(40, 60, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.TopLeft);

            int i = 40;
            foreach (UserPoint pt in points)
            {
                if (!first)
                {
                    pdfPage = pdf.AddPage();
                    graph = XGraphics.FromPdfPage(pdfPage);
                    tf = new XTextFormatter(graph);
                    i = -40;
                }
                else
                    first = false;

                tf.DrawString("Point (" + pt.Latitude.ToString(CultureInfo.InvariantCulture) + " , " + pt.Longitude.ToString(CultureInfo.InvariantCulture) + ")", fontText, XBrushes.Black,
                new XRect(60, 80 + i, 500, pdfPage.Height - i), XStringFormats.TopLeft);

                i += 40;

                tf.DrawString("Name: " + pt.Name, fontText, XBrushes.Black,
                new XRect(80, 80 + i, 500, pdfPage.Height - i), XStringFormats.TopLeft);

                i += 40 + 20 * ((pt.Name.Length + 6)/(500 - 80));

                tf.DrawString("Description: " + pt.Description, fontText, XBrushes.Black,
                new XRect(80, 80 + i, 500, pdfPage.Height - i), XStringFormats.TopLeft);

                /*i += 40 + 40 * ((pt.Description.Length + 13)/(500 - 80) + 5);

                try
                {
                    img = XImage.FromBitmapSource((BitmapSource)pt.Photos[0].Image);
                    graph.DrawImage(img, 20, 80 + i, 175, 175);
                }
                catch (ArgumentOutOfRangeException) { }

                try
                {
                    img = XImage.FromBitmapSource((BitmapSource) pt.Photos[1].Image);
                    graph.DrawImage(img, 220, 80 + i, 175, 175);
                }
                catch (ArgumentOutOfRangeException) { }

                try
                { 
                    img = XImage.FromBitmapSource((BitmapSource) pt.Photos[2].Image);
                    graph.DrawImage(img, 415, 80 + i, 175, 175);
                }
                catch (ArgumentOutOfRangeException) { }
                
                i += 220;*/

                graph.DrawString("� " + DateTime.Now.Year + " AlgoriTeam, Co. - All rights reserved.", fontAlgoriteam, XBrushes.Black,
                new XRect(400, pdfPage.Height.Point-30, pdfPage.Width.Point, pdfPage.Height.Point), XStringFormats.TopLeft);
            }

            return pdf;
        }

        public void Remake_Report()
        {
            StringBuilder aux = new StringBuilder();
            aux.Append("<Activity ref=\"");
            aux.Append(reference);
            aux.Append("\" distance=\"");
            aux.Append(distance.ToString(CultureInfo.InvariantCulture));
            aux.Append("\" experience=\"");
            aux.Append(experience);
            aux.Append("\" points=\"");
            aux.Append(points.Count);
            aux.Append("\">\n");

            foreach (UserPoint pt in points)
            {
                aux.Append("\t<Point latitude=\"");
                aux.Append(pt.Coordinates.Latitude.ToString(CultureInfo.InvariantCulture));
                aux.Append("\" longitude=\"");
                aux.Append(pt.Coordinates.Longitude.ToString(CultureInfo.InvariantCulture));
                aux.Append("\" photos=\"");
                if (pt.Photos != null)
                    aux.Append(pt.Photos.Count);
                else
                    aux.Append("0");
                aux.Append("\">\n");
                aux.Append("\t\t<Name>\n\t\t\t");
                aux.Append(pt.Name);
                aux.Append("\n\t\t</Name>\n\t\t<Description>\n\t\t\t");
                aux.Append(pt.Description);
                aux.Append("\n\t\t</Description>\n\t</Point>\n");
            }

            aux.Append("</Activity>");
            report = aux.ToString();
        }

        public void SortPoints(UserPointComparator c)
        {
            this.points.Sort(c);
        }
    }
}
