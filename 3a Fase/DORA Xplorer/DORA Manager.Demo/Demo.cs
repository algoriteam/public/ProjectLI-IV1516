﻿using Business;
using Data.DAO;
using System;

namespace Data_Layer
{
    public class Demo
    {
        public static void Main(string[] args)
        {
            Business.DORA_Manager man = new Business.DORA_Manager();
            PhotoDAO p = new PhotoDAO();

            var acts = new ActivityDAO();

            Activity act = man.Get_Activity_Photos("#DORAnwKV", "b");
            act.Remake_Report();
            Console.WriteLine(act.Report);
        }
    }
}
