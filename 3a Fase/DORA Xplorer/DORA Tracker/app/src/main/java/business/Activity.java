package business;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Activity {

    // VARIAVEIS
    private String reference;
    private Date date;
    private byte[] voice_register;
    private List<UserPoint> points;
    private double distance;

    // CONSTRUTORES
    public Activity(){
        reference = "";
        date = new Date();
        voice_register = null;
        points = new ArrayList<>();
        distance = 0.0;
    }

    public Activity(String ref, double dist, List<UserPoint> list){
        reference = ref;
        date = new Date();
        voice_register = null;
        points = list;
        distance = dist;
    }

    // GETS / SETS
    public String get_Reference()
    {
        return reference;
    }

    public void set_Reference(String a)
    {
        reference = a;
    }

    public Date get_Date(){ return date; }

    public void set_Date(Date a)
    {
        date = a;
    }

    public byte[] get_Voice()
    {
        return voice_register;
    }

    public void set_Voice(byte[] a)
    {
        voice_register = a;
    }

    public List<UserPoint> get_Points()
    {
        return points;
    }

    public void set_Points(List<UserPoint> a)
    {
        points = a;
    }

    public double get_Distace()
    {
        return distance;
    }

    public void set_Distance(double a)
    {
        distance = a;
    }

    public boolean equals(Object o){
        if (this == o) return true;
        if ((o == null) || (!this.getClass().getName().equals(o.getClass().getName()))) return false;
        else{
            Activity aux = (Activity)o;

            boolean bytes = Arrays.equals(voice_register, aux.voice_register);

            boolean pts = true;
            for (UserPoint pt : points){
                if (!aux.points.contains(pt)){
                    pts = false;
                    break;
                }
            }

            return reference.equals(aux.reference) &&
                    date.equals(aux.date) &&
                    bytes &&
                    pts;
        }
    }
}
