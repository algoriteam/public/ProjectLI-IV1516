package business;

import java.util.ArrayList;
import java.util.List;

public class UserPoint extends Point {

    // VARIAVEIS
    private String name;
    private String description;
    private int num_photos;
    private List<Photo> photos;

    // CONSTRUTORES
    public UserPoint(){
        super.set_Latitude(0.0);
        super.set_Longitude(0.0);
        name = "";
        description = "";
        num_photos = 0;
        photos = new ArrayList<>();
    }

    public UserPoint(double lat, double longi, String n, String d, int num) {
        super.set_Latitude(lat);
        super.set_Longitude(longi);
        name = n;
        description = d;
        num_photos = num;
        photos = new ArrayList<>();
    }

    // GETS / SETS
    public Point get_Coordinates(){
        return new Point(super.get_Latitude(), super.get_Longitude());
    }

    public void set_Coordinates(double lat, double longi)
    {
        super.set_Latitude(lat);
        super.set_Longitude(longi);
    }

    public String get_Name(){
        return name;
    }

    public void set_Name(String a) {
        name = a;
    }

    public String get_Description(){
        return description;
    }

    public void set_Description(String a){
        description = a;
    }

    public int get_Num_Photos(){
        return num_photos;
    }

    public void set_Num_Photos(int a){
        num_photos = a;
    }

    public List<Photo> get_Photos(){
        return photos;
    }

    public void set_Photos(List<Photo> a){
        photos = a;
    }

    // OUTROS METODOS
    public boolean equals(Object o) {
        if (this == o) return true;
        if ((o == null) || (!this.getClass().getName().equals(o.getClass().getName()))) return false;
        else{
            UserPoint aux = (UserPoint) o;
            return super.equals(aux) && name.equals(aux.name) && description.equals(aux.description);
        }
    }
}
