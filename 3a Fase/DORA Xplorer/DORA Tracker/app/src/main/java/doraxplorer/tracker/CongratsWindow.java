package doraxplorer.tracker;

import android.app.Activity;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Locale;

import business.Point;

public class CongratsWindow extends Activity {

    private TextToSpeech DORA;
    private ImageView mRoute;
    private ImageView mDora;
    private ImageView mExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_congrats_window);

        mRoute = (ImageView) findViewById(R.id.route);
        mDora = (ImageView) findViewById(R.id.dora_assistant);
        mExit = (ImageView) findViewById(R.id.exit);

        DORA  = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    DORA.setLanguage(Locale.UK);
                    DORA.setSpeechRate(0.7f);
                    show_dialog("Congratulations, you completed your activity, please be back soon ...");
                }
                else{
                    show_dialog("Error starting DORA Assistant ...");
                }
            }
        });

        mRoute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                setResult(1,resultIntent);
                finish();
            }
        });

        mDora.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                show_dialog("Yes, you finished this activity ...");
            }
        });

        mExit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                setResult(2,resultIntent);
                finish();
            }
        });
    }

    // EXTRAS
    public void show_dialog(String str){
        Toast alert = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        alert.show();
        DORA.speak(str, TextToSpeech.QUEUE_FLUSH, null);
    }
}
