package doraxplorer.tracker;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import business.Activity;
import business.Photo;
import business.Point;
import business.UserPoint;

public class MenuWindow extends android.app.Activity implements OnMapReadyCallback {

    private ImageView mRoute;
    private ImageView mDora;
    private ImageView mExit;
    private GoogleMap mMap;
    private Button mGo;

    // DORA ASSISTANT
    private TextToSpeech DORA;

    // DORA Activity
    private String path;
    private Activity loaded_activity;

    // DORA Activity - Points
    private List<UserPoint> markerPoints;

    // DORA Activity - Voice
    private static int RECORDER_SAMPLE = 8000;
    private byte[] voice;

    private int current_point = 0;
    private Point my_coordinates;
    private Marker my_position;
    private boolean first;

    private int update_location_count;
    private boolean reached;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_menu_window);

        mRoute = (ImageView) findViewById(R.id.route);
        mDora = (ImageView) findViewById(R.id.dora_assistant);
        mExit = (ImageView) findViewById(R.id.exit);
        mGo = (Button) findViewById(R.id.go_button);

        DORA  = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    DORA.setLanguage(Locale.UK);
                    DORA.setSpeechRate(0.7f);
                    show_dialog("Need any help? ...");
                }
                else{
                    show_dialog("Error starting DORA Assistant ...");
                }
            }
        });

        update_location_count = 0;
        reached = true;
        current_point = 0;

        Intent intent = getIntent();
        path = intent.getStringExtra("reference");
        File myFile = new File(path + "/ManagerReport.xml");

        try {
            InputStream stream = new FileInputStream(myFile);
            loaded_activity = syncFromPC(stream);
            stream.close();
        }
        catch(IOException a){
            show_dialog("Activity not found ...");
            loaded_activity = new Activity();
            try {
                Thread.sleep(2000);
            }catch(InterruptedException b){}
            mRoute.callOnClick();
        }
        catch(XmlPullParserException c){
            show_dialog("Error parsing XML file ...");
            try {
                Thread.sleep(2000);
            }catch(InterruptedException d){}
            mRoute.callOnClick();
        }

        markerPoints = loaded_activity.get_Points();
        first = true;

        // MAP OPTIONS
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // LISTENERS
        mRoute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                setResult(1,resultIntent);
                finish();
            }
        });

        mDora.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mDora.setImageResource(R.drawable.dora_awake);
                promptSpeechInput();
            }
        });

        mExit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                setResult(2,resultIntent);
                finish();
            }
        });

        mGo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final UserPoint point = markerPoints.get(current_point);
                if (reached) {
                    // FALSE -> USER DIDNTS REACH THE POINT / TRUE -> USER AUTOMATICALLY REACHES THE POINT (FOR APRESENTATION)
                    reached = false;

                    // START NAVIGATION TO NEXT POINT
                    Thread navigation = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // SHOW NAVIGATION
                            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + point.get_Latitude() + "," + point.get_Longitude() + "&mode=w");
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            startActivity(mapIntent);
                        }
                    });
                    navigation.start();

                    // WAIT FOR USER TO REACH POINT
                    Thread wait = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            boolean cont = true;
                            while (cont) {

                                // GET CURRENT POSITION AND CALCULATE IF USER REACHED THE POINT
                                if (reached) {
                                    cont = false;
                                    // SHOW FORM
                                    Thread form = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            // SHOW FORM
                                            Intent myIntent = new Intent(MenuWindow.this, FormWindow.class);

                                            myIntent.putExtra("point_number", "" + (current_point + 1));
                                            myIntent.putExtra("name", point.get_Name());
                                            myIntent.putExtra("description", point.get_Description());
                                            myIntent.putExtra("num_photos", "" + point.get_Num_Photos());
                                            startActivityForResult(myIntent, 1);
                                        }
                                    });
                                    form.start();
                                }
                                else reached = user_reached_point(my_coordinates, point);


                                try {
                                    Thread.sleep(5000);
                                } catch (InterruptedException e) {}
                            }
                            Thread.currentThread().interrupt();
                        }
                    });
                    wait.start();
                }
                else{
                    show_dialog("You still didn\'t reached point number "+(current_point+1)+" ...");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(resultCode){
            case RESULT_OK:
                switch (requestCode) {
                    case 1:
                        // JUST GOT BACK FROM FORM_WINDOW
                        UserPoint point = markerPoints.get(current_point);

                        byte[] audio = data.getByteArrayExtra("audio");
                        if (audio != null) {
                            if (voice == null) {
                                voice = Arrays.copyOf(audio, audio.length);
                            } else {
                                byte[] destination = new byte[voice.length + audio.length];
                                System.arraycopy(voice, 0, destination, 0, voice.length);
                                System.arraycopy(audio, 0, destination, voice.length, audio.length);

                                voice = Arrays.copyOf(destination, destination.length);
                            }
                        }

                        String name = data.getStringExtra("name");
                        point.set_Name(name);

                        String description = data.getStringExtra("description");
                        point.set_Description(description);

                        int current_num_photos = Integer.parseInt(data.getStringExtra("current_num_photos"));

                        List<Photo> photos = point.get_Photos();
                        int old_num_photos = point.get_Num_Photos();
                        int j = 0;
                        for (int i = old_num_photos; i < current_num_photos; i++) {
                            byte[] picture = data.getByteArrayExtra("photo" + j);
                            photos.add(new Photo("Photo-" + (current_point + 1) + "-" + (i + 1) + ".jpeg", picture));
                            // ADD TEMPORARY IMAGE TO FOLDER
                            try {
                                FileOutputStream bos = new FileOutputStream(path + "/Photo-" + (current_point + 1) + "-" + (i + 1) + ".jpeg");
                                bos.write(picture);
                                bos.flush();
                                bos.close();
                            } catch (IOException e) {
                                show_dialog("Error creating image file ...");
                            }
                            j++;
                        }

                        // CHECK IF FINISHED
                        if (current_point + 1 == markerPoints.size()) {
                            // THE END - SHOW CONGRATULATIONS FORM
                            Intent myIntent = new Intent(MenuWindow.this, CongratsWindow.class);
                            startActivityForResult(myIntent, 3);

                            loaded_activity.set_Date(Calendar.getInstance().getTime());
                            loaded_activity.set_Voice(voice);

                            if (voice != null) {
                                // ADD VOICE TO FOLDER
                                try {
                                    File output = new File(path + "/voice.wav");
                                    rawToWave(voice, output);
                                } catch (IOException e) {
                                    show_dialog("Error creating voice file ...");
                                }
                            }

                            // Update old report with new xml with photo names and data
                            syncToPC(loaded_activity);

                        } else {
                            show_dialog("New point info added ...");
                            current_point++;
                            mGo.setText("GO to point " + (current_point + 1));
                        }
                        break;

                    case 2:
                        if (data != null) {
                            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                            String command = result.get(0);
                            show_dialog(command);
                            if (command.equals("my location")) {
                                LatLng lt_coordinates = new LatLng(my_coordinates.get_Latitude(), my_coordinates.get_Longitude());
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lt_coordinates, 17));
                                show_dialog("You are here ...");
                            } else {
                                if (command.equals("points left")) {
                                    show_dialog("There are " + (markerPoints.size() - current_point) + " points remaining ...");
                                } else {
                                    if (command.equals("points found")) {
                                        show_dialog("You found " + (current_point) + " points until now ...");
                                    } else {
                                        if (command.equals("total points")) {
                                            show_dialog("There are " + markerPoints.size() + " points in total in this activity ...");
                                        } else {
                                            if (command.equals("distance remaining")) {
                                                int size = markerPoints.size();
                                                Point last = markerPoints.get(size - 1);
                                                double distance = my_coordinates.Distance(last);
                                                show_dialog("There are " + String.format("%.3f", distance).replace(",", ".") + " kilometers remaining to reach the next point ...");
                                            } else {
                                                if (command.equals("distance covered")) {
                                                    Point first = markerPoints.get(0);
                                                    Point current = markerPoints.get(current_point);
                                                    double distance = first.Distance(current);
                                                    show_dialog("You walked " + String.format("%.3f", distance).replace(",", ".") + " kilometers in this activity until now ...");
                                                } else {
                                                    if (command.equals("total distance")) {
                                                        double total = loaded_activity.get_Distace();
                                                        show_dialog("The total distance of this activity is " + String.format("%.3f", total).replace(",", ".") + " kilometers ...");
                                                    } else {
                                                        if (command.equals("time")) {
                                                            SimpleDateFormat date_hour = new SimpleDateFormat("HH");
                                                            SimpleDateFormat date_minutes = new SimpleDateFormat("mm");
                                                            Date now = Calendar.getInstance().getTime();
                                                            int hour = Integer.parseInt(date_hour.format(now));
                                                            int minutes = Integer.parseInt(date_minutes.format(now));
                                                            show_dialog("Its " + hour + " hours and " + minutes + " minutes ...");
                                                        } else {
                                                            if (command.equals("exit")) {
                                                                show_dialog("Bye bye fellow explorer ...");
                                                                Thread exit = new Thread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        try {
                                                                            Thread.sleep(3000);
                                                                        } catch (InterruptedException e) {
                                                                        }
                                                                        mExit.callOnClick();
                                                                    }
                                                                });
                                                                exit.start();
                                                            } else {
                                                                if (command.equals("where is texugo")) {
                                                                    LatLng tex_coordinates = new LatLng(41.561619, -8.397375);
                                                                    mMap.addMarker(new MarkerOptions()
                                                                            .position(tex_coordinates)
                                                                            .title("grgrg rg rg rg rgrg")
                                                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.texugo)));

                                                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tex_coordinates, 17));
                                                                    show_dialog("Texugo is hidden somewhere here ...");
                                                                } else {
                                                                    if (command.equals("go to point")) {
                                                                        reached = true;
                                                                    }
                                                                    else show_dialog("I\'m sorry, I\'m not able to complete that request ...");
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        mDora.setImageResource(R.drawable.dora);
                        break;
                }
                break;

            case 1:
                if(requestCode==3) {
                    mRoute.callOnClick();
                }
                break;

            case 2:
                if(requestCode==3) {
                    mExit.callOnClick();
                }
                break;

            default:
                switch (requestCode) {
                    case 1:
                        show_dialog("Error getting form info ...");
                        break;
                    case 2:
                        mDora.setImageResource(R.drawable.dora);
                        show_dialog("Speech recognition canceled ...");
                        break;
                    case 3:
                        mRoute.callOnClick();
                        break;
                }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        UiSettings settings = mMap.getUiSettings();
        settings.setAllGesturesEnabled(true);
        settings.setCompassEnabled(true);
        settings.setRotateGesturesEnabled(true);
        settings.setScrollGesturesEnabled(true);
        settings.setTiltGesturesEnabled(true);
        settings.setZoomControlsEnabled(true);
        settings.setZoomGesturesEnabled(true);

        try {
            mMap.setMyLocationEnabled(true);
        }catch(SecurityException e){}

        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location loc) {

                update_location_count++;
                if (update_location_count%5==0) mMap.clear();
                if(my_position != null) my_position.remove();

                // USER POSITION
                my_coordinates = new Point(loc.getLatitude(), loc.getLongitude());

                LatLng lt_coordinates = new LatLng(my_coordinates.get_Latitude(), my_coordinates.get_Longitude());
                my_position = mMap.addMarker(new MarkerOptions()
                        .position(lt_coordinates)
                        .title("My position")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

                if (first) {
                    if (markerPoints.size() > 0) mGo.setText("GO to point "+(current_point+1));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lt_coordinates, 17));
                }

                if ((update_location_count==1 || update_location_count%5==0) && markerPoints.size() > 0) {
                    Point p_first = my_coordinates;
                    Point p_second = markerPoints.get(0);
                    LatLng first = new LatLng(p_first.get_Latitude(),p_first.get_Longitude());
                    LatLng second = new LatLng(p_second.get_Latitude(),p_second.get_Longitude());

                    String url = getDirectionsUrl(first, second);
                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(url);

                    // MARK FIRST POINT
                    Marker first_position = mMap.addMarker(new MarkerOptions()
                            .position(second)
                            .title("Point number 1")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                    // DRAW FULL ROUTE AND ALL POINT MARKERS
                    int i;
                    for(i=0;i<markerPoints.size()-1;i++) {
                        p_first = markerPoints.get(i);
                        p_second = markerPoints.get(i + 1);
                        first = new LatLng(p_first.get_Latitude(),p_first.get_Longitude());
                        second = new LatLng(p_second.get_Latitude(),p_second.get_Longitude());

                        Marker second_position = mMap.addMarker(new MarkerOptions()
                                .position(second)
                                .title("Point number " + (i+2))
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                        url = getDirectionsUrl(first, second);
                        downloadTask = new DownloadTask();
                        downloadTask.execute(url);
                    }
                }
                first = false;
            }
        });
    }

    // CREATE AUDIO FILE FROM RAW DATA
    private void rawToWave(byte[] rawData, File waveFile) throws IOException {
        DataOutputStream output = null;
        try {
            output = new DataOutputStream(new FileOutputStream(waveFile));

            // WAVE HEADER
            writeString(output, "RIFF"); // chunk id
            writeInt(output, 36 + rawData.length); // chunk size
            writeString(output, "WAVE"); // format
            writeString(output, "fmt "); // subchunk 1 id
            writeInt(output, 16); // subchunk 1 size
            writeShort(output, (short) 1); // audio format (1 = PCM)
            writeShort(output, (short) 1); // number of channels
            writeInt(output, RECORDER_SAMPLE); // sample rate
            writeInt(output, RECORDER_SAMPLE * 2); // byte rate
            writeShort(output, (short) 2); // block align
            writeShort(output, (short) 16); // bits per sample
            writeString(output, "data"); // subchunk 2 id
            writeInt(output, rawData.length); // subchunk 2 size
            output.write(rawData);

        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    private void writeInt(final DataOutputStream output, final int value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
        output.write(value >> 16);
        output.write(value >> 24);
    }

    private void writeShort(final DataOutputStream output, final short value) throws IOException {
        output.write(value >> 0);
        output.write(value >> 8);
    }

    private void writeString(final DataOutputStream output, final String value) throws IOException {
        for (int i = 0; i < value.length(); i++) {
            output.write(value.charAt(i));
        }
    }

    // SPEECH INPUT
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.UK);
        try {
            startActivityForResult(intent, 2);
        } catch (ActivityNotFoundException a) {
            show_dialog("Sorry, but your device doesn\'t support speech input ...");
        }
    }

    // EXTRAS
    public boolean user_reached_point(Point user, Point point){
        double distance = user.Distance(point);
        return distance - 0.02 < 0;
    }

    public void show_dialog(String str){
        Toast alert = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        alert.show();
        DORA.speak(str, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void show_dialog(String str, Context c){
        Toast alert = Toast.makeText(c, str, Toast.LENGTH_SHORT);
        alert.show();
        DORA.speak(str, TextToSpeech.QUEUE_FLUSH, null);
    }

    // PARSE XML FILE
    public void syncToPC(Activity my){
        String builder = parseActivity(my);
        System.out.println(builder);

        try{
            File old_report = new File(path + "/ManagerReport.xml");
            old_report.delete();

            File new_report = new File(path + "/TrackerReport.xml");
            PrintWriter writer = new PrintWriter(new_report);
            writer.println(builder);
            writer.flush();
            writer.close();
        }
        catch(FileNotFoundException e){
            show_dialog("File not found exception ...");
        }

    }

    public String parseActivity(Activity my){
        SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yyyy");
        StringBuilder sb = new StringBuilder();

        String reference = my.get_Reference();
        String data =  date_format.format(my.get_Date());
        int num_points = my.get_Points().size();
        double distance = my.get_Distace();

        sb.append("<Activity ref=\"");
        sb.append(reference);
        sb.append("\" points=\"");
        sb.append(num_points);
        sb.append("\" date=\"");
        sb.append(data);
        sb.append("\" distance=\"");
        sb.append(distance);
        sb.append("\" voice=\"");
        sb.append(voice != null);
        sb.append("\">\n");

        // FOR EACH POINT
        for(UserPoint p : my.get_Points()) {

            double lat = p.get_Latitude();
            double longi = p.get_Longitude();
            String name = p.get_Name();
            String description = p.get_Description();

            sb.append("\t<Point latitude=\"");
            sb.append(lat);
            sb.append("\" longitude=\"");
            sb.append(longi);
            sb.append("\" photos=\"");
            sb.append(p.get_Photos().size());
            sb.append("\">\n");

            sb.append("\t\t<Name>");
            sb.append("\n\t\t\t"+name);
            sb.append("\n\t\t</Name>");

            sb.append("\n\t\t<Description>");
            sb.append("\n\t\t\t"+description);
            sb.append("\n\t\t</Description>");

            sb.append("\n\t\t<Photos>\n");

            // FOR EACH PHOTO
            for (Photo f : p.get_Photos()) {
                String le_name = f.get_Name();
                sb.append("\t\t\t<Photo name=\"");
                sb.append(le_name);
                sb.append("\"/>\n");
            }
            sb.append("\t\t</Photos>");
            sb.append("\n\t</Point>\n");
        }

        sb.append("</Activity>");
        return sb.toString();
    }

    public Activity syncFromPC(InputStream  in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readActivity(parser);
        } finally {
            in.close();
        }
    }

    private Activity readActivity(XmlPullParser parser) throws IOException, XmlPullParserException {
        Activity result = new Activity();
        parser.require(XmlPullParser.START_TAG, null, "Activity");
        String reference = parser.getAttributeValue(null, "ref");
        result.set_Reference(reference);
        double distance = Double.parseDouble(parser.getAttributeValue(null, "distance"));
        result.set_Distance(distance);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Point")) {
                UserPoint point = readPoint(parser);
                result.get_Points().add(point);
            } else {
                skip(parser);
            }
        }
        return result;
    }

    private UserPoint readPoint(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "Point");
        double latitude = Double.parseDouble(parser.getAttributeValue(null, "latitude"));
        double longitude = Double.parseDouble(parser.getAttributeValue(null, "longitude"));
        int num_photos = Integer.parseInt(parser.getAttributeValue(null, "photos"));
        String name = "";
        String description = "";
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String nname = parser.getName();
            if(nname.equals("Name")) name = readName(parser);
            else {
                if(nname.equals("Description")) description = readDescription(parser);
                else skip(parser);
            }
        }
        return new UserPoint(latitude, longitude, name, description, num_photos);
    }

    // Processes name tags in the feed.
    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "Name");
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "Name");
        return name;
    }

    // Processes decription tags in the feed.
    private String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "Description");
        String description = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, "Description");
        return description;
    }

    // For the tags name and description
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    // CALCULATE ROUTE POLYLINE
    // Directions
    public class Directions {

        public List<List<HashMap<String,String>>> parse(JSONObject jObject){

            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;

            try {

                jRoutes = jObject.getJSONArray("routes");


                for(int i=0;i<jRoutes.length();i++){
                    jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
                    List path = new ArrayList<HashMap<String, String>>();


                    for(int j=0;j<jLegs.length();j++){
                        jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");


                        for(int k=0;k<jSteps.length();k++){
                            String polyline = "";
                            polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);


                            for(int l=0;l<list.size();l++){
                                HashMap<String, String> hm = new HashMap<String, String>();
                                hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                                hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }catch (Exception e){
            }
            return routes;
        }

        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }
            return poly;
        }
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=true";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){

        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String>{

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

                ParserTask parserTask = new ParserTask();

                // Invokes the thread for parsing the JSON data
                parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                Directions parser = new Directions();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            try {
                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(5);
                    lineOptions.color(Color.rgb(58, 91, 226));
                }

                // Drawing polyline in the Google Map for the i-th route
                mMap.addPolyline(lineOptions);
            }catch(NullPointerException e){
                show_dialog("Connection error, couldn\'t fetch activity route, but points are still visible ...");
            }
        }
    }
}
