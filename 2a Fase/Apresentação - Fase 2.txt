APRESENTA��O: (-> Slide; + Conteudo a falar)

-> Inicio
	+ Falar muito resumidamente daquilo que foi concluido da 1� fase, e o que foi estipulado fazer para a 2� fase


-> Plataformas
	+ Falar sobre as plataformas a construir, e como foram formulados os requisitos - CONSULTAR SEMINARIOS Requirements Engineering (tipos de requerimentos, stakeholder, tecnicas de valida��o)
	+ Falar sobre a necessidade de construir diagramas que interpretem aquilo que ser� realizado - CONSULTAR SEMINARIOS Requirements Engineering (System Modeling)
	+ Falar sobre Assistente nas duas plataformas (ou talvez seja um assunto para falar ao longo da apresenta��o quando surgir)
	+ Falar sobre os perfis de utiliza��o (no contexto geral)


-> Base de Dados
	+ Muito Resumidamente falar do m�todo adotado para planeamento da Base de Dados
	+ Falar sobre os perfis de utiliza��o (no contexto de BD)

	-> Modelo Conceptual
		+ Falar sobre entidades atributos e relacionamentos existentes
		
	-> Modelo L�gico
		+ Falar sobre rela��es estabelecidas a partir do Modelo Conceptual
		+ Dizer que ap�s a sua constru��o foi feita uma valida��o do modelo recorrendo � Normaliza��o


-> Modela��o UML

	-> Modelo de Dom�nio (A ilustra��o da informa��o importante para a constru��o do sistema)
		+ Falar sobre o que representa este diagrama e a importancia da sua constru��o

	-> Diagrama de Use Case (A ilustra��o de tarefas que o utilizador poder� realizar no sistema)
		+ Mostrar dois slides distintos com os diagramas de cada plataforma e idealizar fun��es mais importante
		+ Falar que para cada use case foi definido a sua especifica��es, um diagrama de Atividade, e um diagrama de sequ�ncia ( e que a sua demonstra��o ser� aqui realizada apenas com o m�todo de realizar atividade (que � o m�todo mais relevante em todo o sistema) - CONSULTAR SEMINARIOS Requirements Engineering (System Modeling)

	-> Especifica��o de Use Case (Especifica��o detalhada de uma fun��o disponibilizada ao utilizador pelo Sistema)
		+ Falar que m�todo de especifica��o foi usado e o que define num use case usando o exemplo mostrado
	
	-> Diagrama de Atividade (Ilustra��o da itera��o do utilizador-sistema na execu��o da fun��o)
		+ Falar da import�ncia de apresentar um diagrama de atividade por cada use case
		+ Ilustrar funcionamento da fun��o apresentada como exemplo

	-> Diagrama de Sequencia (Ilustra��o do funcionamento interno da fun��o)
		+ Falar da import�ncia de apresentar um diagrama de sequencia por cada use case
		+ Falar que a sua execu��o foi em forma de pseudo c�digo
		+ Ilustrar funcionamento da fun��o apresentada como exemplo
	
	-> Diagrama de Classes (visualiza��o geral das "fun��es" envolvidas no funcionamento interno do sistema)
		+ Falar em pontos mais importantes a real�ar no diagrama (a existencia de uma facade que ser� a classe principal do sistema e as liga��es que este tem com as DAOs e a liga��o dessas DAOs com as suas classes atrav�s de Maps)
		+ ATEN��O que s� fizemos classes para a plataforma Manager	

	-> M�quinas de Estado (ilustra��o de estados do sistema durante a sua execu��o)
		+ Mostrar dois slides distintos com os diagramas de cada plataforma e idealizar estados que os programa poder�o entrar consoante as atividades do utilizador
	
	-> Diagrama de Instala��o (Ilustra��o de requerimentos de hardware e software)
		+ Falar sobre os requerimentos de hardware e software que o sistema em constru��o precisar� e funcionamento entre si.
	
	-> Ultimo Slide (que � igual ao primeiro)
		+ Concluir apresenta��o referindo a experi�ncia recebida desta fase e trabalho futuro
