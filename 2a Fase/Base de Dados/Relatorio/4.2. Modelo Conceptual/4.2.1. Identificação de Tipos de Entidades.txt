
Utilizador

    Descrição: Termo geral que descreve os utilzadores do sistema
    Aliases: Explorador
    Ocorrência: O utilizador é a pessoa que vai utilizar o sistema, ou seja, vai criar sessões e descobrir pontos. Este vai estar sempre ligado às suas sessões, mantendo assim o histórico das mesmas. Pode ainda adicionar um nome ao ponto, uma descrição e fotos (até um máximo de 3).

Ponto

    Descrição: Termo geral que descreve os pontos de interesse adicionados pelos utilizadores no sistema
    Aliases: Local
    Ocorrência: Um ponto de interesse é um local que contém algo que chama a atenção dos utilizadores, ou seja, tem algo que pode ser visto, admirado e, no caso do nosso sistema, descrito e fotografado. Estes pontos apenas são adicionados à base de dados aquando da sua descoberta por parte de um utilizador, sendo que pontos não descobertos por exploradores não existirão na base de dados.

Sessão

    Descrição: Termo geral que designa as sessões presentes no sistema
    Aliases: Atividade, Percurso
    Ocorrência: Uma sessão é uma atividade de campo que contém um percurso que passa por vários pontos de interesse e que, na conclusão da mesma, vai conter todas as informações que o utilizador que realizou a sessão adicionou aquando da realização da mesma, desde descrições dos pontos de interesse presentes na sessão até às fotos tiradas dos mesmos. Pressupõe-se que todas as sessões que estão na base de dados estão concluídas, sendo que estas apenas são adicionadas na mesma aquando da sua sincronização com a plataforma móvel.
