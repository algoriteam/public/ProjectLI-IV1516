*** CARACTERIZAÇÃO DOS PERFIS DE UTILIZAÇÃO ***

=> ADMINISTRADOR

    Este elemento terá a responsabilidade de gerir todos os dados existentes na BD recorrendo a um SGBD, neste caso o SQL Server.
    Terá a preocupação de criar, preencher, atualizar e até mesmo eliminar todo o conteúdo vivo da BD. 
    Refere – se assim basicamente à lista de utilizadores, sessões, pontos e todas as relações existentes entre estes que serão clarificadas de forma mais explícita na secção ... desde documento.

=> UTILIZADOR

    Principal elemento e centro objetivo do desenvolvimento deste sistema/ferramenta. O utilizador aqui referido terá, idealmente, a possibilidade de consultar a lista das suas sessões, tendo acesso à informação de cada uma (Código de referência da sessão e conjunto de pontos selecionados) e todas as outras entidades relacionadas, tal como os pontos de interesse no sistema.
    Adicionalmente o utilizador poderá ainda interagir com estes recursos, desde a simples leitura/processamento da informação até à possibilidade de inserir uma nova sessão ou ponto na BD.
    Introduzimos ainda na plataforma um perfil para cada utilizador (nome, email, morada (Incluindo coordenadas), contactos e imagem de perfil), onde cada um utilizará o seu correio eletrónico e palava – passe para realizar login no sistema.
