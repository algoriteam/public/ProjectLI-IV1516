Laboratórios Informática IV

Título:

    Dora Xplorer - Destination Oriented Responsive Assistant Explorer - Assistente de campo + Plataforma Desktop (Manager) + Android Mobile (Tracker)

Autores:

    A70377  André Filipe Proença e Silva
    A71184  João Pedro Pereira Fontes
    A64309  Pedro Vieira Fortes
    A69857  Vitaliy Parytskyy


Contato Email:
{a70377,a71184,a64309,a69857}@alunos.uminho.pt
